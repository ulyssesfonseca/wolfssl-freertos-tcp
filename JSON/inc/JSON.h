/*
 *	Biblioteca criada para criar e fazer parse de JSON em c.
 * 
 *	author: Ulysses Fonseca
 * 	date: September 2019
 * 	version: 1.0.0
 */
#ifndef __JSON__
#define __JSON__

#include <stdio.h>
#include <stdint.h>
#include <string.h>


#define JSON_VERSION 1.0.1

enum{
	OPEN_CONTAINER,
	CLOSE_CONTAINER,
	OPEN_MATRIX,
	CLOSE_MATRIX
};


/*
 * Esta funcao deve ser criada na aplicacao para sempre ser chamada quando uma nova variavel for identificada na string json.
 *	
 * param:
 * char* pVar 	- variavel json
 * char* pValue 	- valor da variavel json
 * int pIndex	- index da variavel na string json
 * char pSub	- variavel cria um sub container
 * 		0 - variable with value
 * 		1 - open container
 * 		2 - close container
 * 		3 - open matrix
 * 		4 - close matrix
 */
extern void json_call_field(char* pVar, char* pValue, int pIndex, char pSub);


/*
 * Funcao realiza o parse da string json.
 *
 * param:
 * char* pString 	- payload a ser analizado
 * int pVariable	- 0 varre toda a string, diferente de zero varre até o index especificado
 */
void json_parse(char* pString, int pVariable);


/*
 * Limpa o buffer do body do json.
*/
void json_clear(void);
/*
 * Insere uma nova variavel com valor do tipo string.
 *
 * param:
 * char* pVariable	- nome da variavel
 * char* pValue	- valor da variavel
 */
void json_insertStr(char* pVariable, char* pValue);
/*
 * Insere uma nova variavel com valor do tipo inteiro.
 *
 * param:
 * char* pVariable	- nome da variavel
 * int pValue	- valor da variavel
 */
void json_insertInt(char* pVariable, int pValue);



/*
 * Abre uma variavel do tipo container para insercao.
 *
 * param:
 * char* pVariable	- nome da variavel
 */
void json_openContainerStr(char* pContainer);
/*
 * Abre uma variavel do tipo container para insercao.
 *
 */
void json_openContainer(void);
/*
 * Fecha o container corrente.
 */
void json_closeContainer(void);




/*
 * Abre uma variavel do tipo matriz para insercao.
 *
 * param:
 * char* pVariable	- nome da variavel
 */
void json_openMatrixStr(char* pMatrix);
/*
 * Abre uma variavel do tipo container para insercao.
 *
 */
void json_openMatrix(void);
/*
 * Fecha o container corrente.
 */
void json_closeMatrix(void);



/*
 * Abre o objeto json.
 */
void json_open(void);
/*
 * Fecha o objeto json.
 */
void json_close(void);

#endif


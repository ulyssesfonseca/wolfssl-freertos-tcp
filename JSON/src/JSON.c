
#include "JSON.h"

extern char json_body[4*1024];
extern char json_variable[20];
extern char json_value[40];

char json_sub = 0;
char json_print_virgula = 0;

void json_parse(char* pString, int pVariable){
	int i = 0;
	int j = 0;
//	int field = 0;
	char start = 0;
	char inside = 0;
	char container = 0;
	int variable = 0;
	int value = 0;

	for(i=0; i<strlen((char*)pString); i++){
		if(pString[i]=='"'){
			//field++;
			j=0;
			start = !start;
			if(start){
				//if(value==1){variable++;}
				memset(&json_value,0x00,sizeof(json_value));
				/*if(pString[i-2]==':'){value=1;}
				else{value=0;}*/
			}
			else{
				if(value==0){variable++;}
				//if(pString[i+3]=='{'){inside=1;}else{inside=0;}
				if(pVariable==variable && value && pVariable!=0)break;
				if(value){
					json_call_field(json_variable,json_value,variable,container);
					memset(&json_value,0x00,sizeof(json_value));
					memset(&json_variable,0x00,sizeof(json_variable));
				}
				value=0;
			}
			//printf("%s %d %d\n",json_variable,value,variable);
		}
		else if(pString[i]=='{'){container=1; value=0;}
		else if(pString[i]=='}'){container=2;}
		else if(pString[i]=='['){container=3;}
		else if(pString[i]==']'){container=4;}
		//else if(pString[i]==':' && start!=1){start=1;}
		else if(pString[i]==':'){value=1;}
		else{
			if(start){
				if(value){ 	json_value[j] = pString[i]; json_value[j+1]='\0';}
				else{		json_variable[j] = pString[i]; json_variable[j+1]='\0';}
				//printf("\n%d %s %s",value,json_value,json_variable);
				j++;
			}
		}

		if(container){
			json_call_field(json_variable,json_value,variable,container);
			container = 0;
		}
	}
}


void json_clear(void){
	memset(&json_body,0x00,sizeof(json_body));
	json_print_virgula = 0;
}

void _json_print_virgula(void){
	char temp[3];
	if(json_print_virgula){
		snprintf(temp,sizeof(temp),",");
		strcat(json_body,temp);
	}
}

void json_insertStr(char* pVariable, char* pValue){
	char temp[100];

	_json_print_virgula();
	snprintf(temp,sizeof(temp),"\"%s\":\"%s\"",pVariable,pValue);

	json_print_virgula = 1;
	strcat(json_body,temp);
}
void json_insertInt(char* pVariable, int pValue){
	char temp[100];
	snprintf(temp,sizeof(temp),"%d",pValue);
	json_insertStr(pVariable,temp);
}

void json_openContainerStr(char* pContainer){
	char virgula = 0;
	char temp[100];

	_json_print_virgula();

	json_sub = 1;
	snprintf(temp,sizeof(temp),"\"%s\":{",pContainer);
	json_print_virgula = 0;
	strcat(json_body,temp);
}
void json_openContainer(void){
	char virgula = 0;
	char temp[100];

	_json_print_virgula();

	json_sub = 1;
	snprintf(temp,sizeof(temp),"{");
	json_print_virgula = 0;
	strcat(json_body,temp);
}
void json_closeContainer(void){
	json_close();
}

void json_openMatrixStr(char* pMatrix){
	char virgula = 0;
	char temp[100];

	_json_print_virgula();

	json_sub = 1;
	snprintf(temp,sizeof(temp),"\"%s\":[",pMatrix);
	json_print_virgula = 0;
	strcat(json_body,temp);
}
void json_openMatrix(void){
	char virgula = 0;
	char temp[100];

	_json_print_virgula();

	json_sub = 1;
	snprintf(temp,sizeof(temp),"[");
	json_print_virgula = 0;
	strcat(json_body,temp);
}
void json_closeMatrix(void){
	strcat(json_body,"]");
	json_print_virgula=1;
}

void json_open(void){
	strcat(json_body,"{");
	json_print_virgula=0;
}
void json_close(void){
	strcat(json_body,"}");
	json_print_virgula=1;
}

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../app/SEGGERDEMO/GUIDEMO.c \
../app/SEGGERDEMO/GUIDEMO_Automotive.c \
../app/SEGGERDEMO/GUIDEMO_BarGraph.c \
../app/SEGGERDEMO/GUIDEMO_Bitmap.c \
../app/SEGGERDEMO/GUIDEMO_ColorBar.c \
../app/SEGGERDEMO/GUIDEMO_Cursor.c \
../app/SEGGERDEMO/GUIDEMO_Fading.c \
../app/SEGGERDEMO/GUIDEMO_Graph.c \
../app/SEGGERDEMO/GUIDEMO_IconView.c \
../app/SEGGERDEMO/GUIDEMO_ImageFlow.c \
../app/SEGGERDEMO/GUIDEMO_Intro.c \
../app/SEGGERDEMO/GUIDEMO_Listview.c \
../app/SEGGERDEMO/GUIDEMO_Resource.c \
../app/SEGGERDEMO/GUIDEMO_Skinning.c \
../app/SEGGERDEMO/GUIDEMO_Speed.c \
../app/SEGGERDEMO/GUIDEMO_Speedometer.c \
../app/SEGGERDEMO/GUIDEMO_TransparentDialog.c \
../app/SEGGERDEMO/GUIDEMO_Treeview.c \
../app/SEGGERDEMO/GUIDEMO_VScreen.c \
../app/SEGGERDEMO/GUIDEMO_WashingMachine.c \
../app/SEGGERDEMO/GUIDEMO_ZoomAndRotate.c \
../app/SEGGERDEMO/IMAGE_SeggerLogo_300x150_565c.c \
../app/SEGGERDEMO/SEGGERDEMO.c 

OBJS += \
./app/SEGGERDEMO/GUIDEMO.o \
./app/SEGGERDEMO/GUIDEMO_Automotive.o \
./app/SEGGERDEMO/GUIDEMO_BarGraph.o \
./app/SEGGERDEMO/GUIDEMO_Bitmap.o \
./app/SEGGERDEMO/GUIDEMO_ColorBar.o \
./app/SEGGERDEMO/GUIDEMO_Cursor.o \
./app/SEGGERDEMO/GUIDEMO_Fading.o \
./app/SEGGERDEMO/GUIDEMO_Graph.o \
./app/SEGGERDEMO/GUIDEMO_IconView.o \
./app/SEGGERDEMO/GUIDEMO_ImageFlow.o \
./app/SEGGERDEMO/GUIDEMO_Intro.o \
./app/SEGGERDEMO/GUIDEMO_Listview.o \
./app/SEGGERDEMO/GUIDEMO_Resource.o \
./app/SEGGERDEMO/GUIDEMO_Skinning.o \
./app/SEGGERDEMO/GUIDEMO_Speed.o \
./app/SEGGERDEMO/GUIDEMO_Speedometer.o \
./app/SEGGERDEMO/GUIDEMO_TransparentDialog.o \
./app/SEGGERDEMO/GUIDEMO_Treeview.o \
./app/SEGGERDEMO/GUIDEMO_VScreen.o \
./app/SEGGERDEMO/GUIDEMO_WashingMachine.o \
./app/SEGGERDEMO/GUIDEMO_ZoomAndRotate.o \
./app/SEGGERDEMO/IMAGE_SeggerLogo_300x150_565c.o \
./app/SEGGERDEMO/SEGGERDEMO.o 

C_DEPS += \
./app/SEGGERDEMO/GUIDEMO.d \
./app/SEGGERDEMO/GUIDEMO_Automotive.d \
./app/SEGGERDEMO/GUIDEMO_BarGraph.d \
./app/SEGGERDEMO/GUIDEMO_Bitmap.d \
./app/SEGGERDEMO/GUIDEMO_ColorBar.d \
./app/SEGGERDEMO/GUIDEMO_Cursor.d \
./app/SEGGERDEMO/GUIDEMO_Fading.d \
./app/SEGGERDEMO/GUIDEMO_Graph.d \
./app/SEGGERDEMO/GUIDEMO_IconView.d \
./app/SEGGERDEMO/GUIDEMO_ImageFlow.d \
./app/SEGGERDEMO/GUIDEMO_Intro.d \
./app/SEGGERDEMO/GUIDEMO_Listview.d \
./app/SEGGERDEMO/GUIDEMO_Resource.d \
./app/SEGGERDEMO/GUIDEMO_Skinning.d \
./app/SEGGERDEMO/GUIDEMO_Speed.d \
./app/SEGGERDEMO/GUIDEMO_Speedometer.d \
./app/SEGGERDEMO/GUIDEMO_TransparentDialog.d \
./app/SEGGERDEMO/GUIDEMO_Treeview.d \
./app/SEGGERDEMO/GUIDEMO_VScreen.d \
./app/SEGGERDEMO/GUIDEMO_WashingMachine.d \
./app/SEGGERDEMO/GUIDEMO_ZoomAndRotate.d \
./app/SEGGERDEMO/IMAGE_SeggerLogo_300x150_565c.d \
./app/SEGGERDEMO/SEGGERDEMO.d 


# Each subdirectory must supply rules for building sources it contributes
app/SEGGERDEMO/%.o: ../app/SEGGERDEMO/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DNDEBUG -D__REDLIB__ -D__USE_CMSIS -D__CODE_RED -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\cmsis\inc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\freertos\include" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\freertos\portable\gcc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\GUI" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\Inc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\Config" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\System\HW" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\app\inc" -Os -Os -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



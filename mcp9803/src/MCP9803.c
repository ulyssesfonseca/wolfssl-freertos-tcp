
#include "MCP9803.h"
#include "chip.h"


//-----------------------------------------------------------------------------
//	SENSOR DE TEMPERATURA
//-----------------------------------------------------------------------------
uint16_t MCP9803_Read_register (uint8_t adress, uint8_t pointer){
	uint8_t buf[2];

	Chip_I2C_MasterSend(I2C0, adress>>1, &pointer, 1);
	Chip_I2C_MasterRead(I2C0, adress>>1, buf, 2);


	return ((buf[0]<<8)|buf[1])>>7;
}

/***************************************************************************************************************
@autor Ulysses C. Fonseca

@descricao
	Realiza acesso ao chip MCP9803(0x90), sensor de temperatura

@param
	-U8 adress _ endereço do chip
	-U8 pointer_ endereço de memória desejado
	_U8 data   _ dado a ser gravado

 ***************************************************************************************************************/
void MCP9803_Write_register(uint8_t adress, uint8_t pointer, uint8_t data){
	uint8_t buf[2];

	buf[0] = pointer;
	buf[1] = data;
	Chip_I2C_MasterSend(I2C0, adress>>1, buf, 2);

}

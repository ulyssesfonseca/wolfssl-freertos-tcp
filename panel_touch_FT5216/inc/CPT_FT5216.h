/***************************************************************************************
	Versao: 001 26/01/2017
	Autor: Ulysses C. Fonseca
	Empresa: Embtech Tecnologia Embarcada SA
	
	Driver de comunica��o com o controlador Panel Touch FT5216
	
****************************************************************************************/

#ifndef __TOUCH_CPT_FT5216__
#define __TOUCH_CPT_FT5216__

#include <stdint.h>
//#include "lpc177x_8x_gpio.h"



#define _TC_I2C_BASE_ADDR 				0x4001C000
#define _TC_I2C_PERIPHERAL_CLOCK 	PeripheralClock
#define _TC_I2C_CLOCK_RATE			 	100000


#define _TC_BASE_ADDRESS			0x70

//------------------------------------------------------------------------------------
//					REGISTERS OPERATING MODE
//------------------------------------------------------------------------------------
#define _TC_ADDR_DEVICE_MODE	0x00
#define _TC_ADDR_GEST_ID			0x01
#define _TC_ADDR_TD_STATUS		0x02

#define _TC_ADDR_TOUCH1_XH		0x03
#define _TC_ADDR_TOUCH1_XL		0x04
#define _TC_ADDR_TOUCH1_YH		0x05
#define _TC_ADDR_TOUCH1_YL		0x06

#define _TC_ADDR_TOUCH2_XH		0x09
#define _TC_ADDR_TOUCH2_XL		0x0A
#define _TC_ADDR_TOUCH2_YH		0x0B
#define _TC_ADDR_TOUCH2_YL		0x0C

#define _TC_ADDR_TOUCH3_XH		0x0F
#define _TC_ADDR_TOUCH3_XL		0x10
#define _TC_ADDR_TOUCH3_YH		0x11
#define _TC_ADDR_TOUCH3_YL		0x12

#define _TC_ADDR_TOUCH4_XH		0x15
#define _TC_ADDR_TOUCH4_XL		0x16
#define _TC_ADDR_TOUCH4_YH		0x17
#define _TC_ADDR_TOUCH4_YL		0x18

#define _TC_ADDR_TOUCH5_XH		0x1B
#define _TC_ADDR_TOUCH5_XL		0x1C
#define _TC_ADDR_TOUCH5_YH		0x1D
#define _TC_ADDR_TOUCH5_YL		0x1E

#define _TC_ADDR_ID_G_THGROUP 0x80
#define _TC_ADDR_ID_G_THPEAK	0x81
#define _TC_ADDR_ID_G_THCAL		0x82
#define _TC_ADDR_ID_G_THWATER		0x83
#define _TC_ADDR_ID_G_THTEMP	0x84
#define _TC_ADDR_ID_G_THDIFF	0x85
#define _TC_ADDR_ID_G_CTRL		0x86
#define _TC_ADDR_ID_G_TIME_ENTER_MONITOR	0x87
#define _TC_ADDR_ID_G_PERIOD_ACTIVE	0x88
#define _TC_ADDR_ID_G_PERIOD_MONITOR 0x89

#define _TC_ADDR_ID_G_AUTO_CLB_MODE		0xA0
#define _TC_ADDR_ID_G_LIB_VERSION_H		0xA1
#define _TC_ADDR_ID_G_LIB_VERSION_L		0xA2
#define _TC_ADDR_ID_G_CIPHER		0xA3
#define _TC_ADDR_ID_G_MODE		0xA4
#define _TC_ADDR_ID_G_PMODE		0xA5
#define _TC_ADDR_ID_G_FIRMID	0xA6
#define _TC_ADDR_ID_G_STATE		0xA7
#define _TC_ADDR_ID_G_FT5201ID	0xA8
#define _TC_ADDR_ID_G_ERR			0xA9
#define _TC_ADDR_ID_G_CLB			0xAA

#define _TC_ADDR_ID_G_B_AREA_TH		0xAE





//#define TOUCH_RESET_IOCON	LPC_IOCON->P0_20
//#define TOUCH_RESET_BIT		20
//#define TOUCH_RESET_PORT 	LPC_GPIO0
//#define TOUCH_RESET_READ  (TOUCH_RESET_PORT->PIN&(1<<TOUCH_RESET_BIT))
//#define TOUCH_RESET_SET   (TOUCH_RESET_PORT->SET|=(1<<TOUCH_RESET_BIT))
//#define TOUCH_RESET_CLR   (TOUCH_RESET_PORT->CLR|=(1<<TOUCH_RESET_BIT))
//
//#define TOUCH_WAKE_IOCON	LPC_IOCON->P0_23
//#define TOUCH_WAKE_BIT		23
//#define TOUCH_WAKE_PORT 	LPC_GPIO0
//#define TOUCH_WAKE_READ  (TOUCH_WAKE_PORT->PIN&(1<<TOUCH_WAKE_BIT))
//#define TOUCH_WAKE_SET   (TOUCH_WAKE_PORT->SET|=(1<<TOUCH_WAKE_BIT))
//#define TOUCH_WAKE_CLR   (TOUCH_WAKE_PORT->CLR|=(1<<TOUCH_WAKE_BIT))
//
//#define TOUCH_EINT_IOCON	LPC_IOCON->P0_24
//#define TOUCH_EINT_BIT		24
//#define TOUCH_EINT_PORT 	LPC_GPIO0
//#define TOUCH_EINT_READ  (TOUCH_EINT_PORT->PIN&(1<<TOUCH_EINT_BIT))?0:1
//#define TOUCH_EINT_SET   (TOUCH_EINT_PORT->SET|=(1<<TOUCH_EINT_BIT))
//#define TOUCH_EINT_CLR   (TOUCH_EINT_PORT->CLR|=(1<<TOUCH_EINT_BIT))


typedef struct{
	uint8_t	 Pressed;
	
	uint8_t	 X1_Flag;
	uint8_t	 X2_Flag;
	uint8_t	 X3_Flag;
	uint8_t	 X4_Flag;
	uint8_t	 X5_Flag;
	uint8_t	 Y1_Flag;
	uint8_t	 Y2_Flag;
	uint8_t	 Y3_Flag;
	uint8_t	 Y4_Flag;
	uint8_t	 Y5_Flag;
	
	uint16_t X1;
	uint16_t X2;
	uint16_t X3;
	uint16_t X4;
	uint16_t X5;
	uint16_t Y1;
	uint16_t Y2;
	uint16_t Y3;
	uint16_t Y4;
	uint16_t Y5;
	
	uint8_t GEST_ID;
	uint8_t TD_STATUS;
	uint8_t DEVICE_MODE;
	uint8_t ID_G_THGROUP;
	uint8_t ID_G_THPEAK;
	uint8_t ID_G_THCAL;
	uint8_t ID_G_THWATER;
	uint8_t ID_G_THDIFF;
	uint8_t ID_G_THTEMP;
	uint8_t ID_G_CTRL;
	uint8_t ID_G_TIME_ENTER_MONITOR;
	uint8_t ID_G_PERIOD_ACTIVE;
	uint8_t ID_G_PERIOD_MONITOR;
	uint8_t ID_G_AUTO_CLB_MODE;
	uint16_t ID_G_LIB_VERSION;
	uint8_t VENDOR_ID;
	uint8_t ID_G_MODE;
	uint8_t ID_G_PMODE;
	uint8_t ID_G_FIRMID;
	uint8_t ID_G_STATE;
	uint8_t ID_G_FT5201ID;
	uint8_t ID_G_ERR;
	uint8_t ID_G_CLB;
	uint8_t ID_G_B_AREA_TH;
}CPT_PID_STATE;
 

void CPT_FT5216_Read_DEVICE_MODE(void);
void CPT_FT5216_Read_GEST_ID(void);
void CPT_FT5216_Read_TD_STATUS(void);
void CPT_FT5216_Read_XY1(void);
void CPT_FT5216_Read_XY2(void);
void CPT_FT5216_Read_XY3(void);
void CPT_FT5216_Read_XY4(void);
void CPT_FT5216_Read_XY5(void);
void CPT_FT5216_Read_ID_G_THGROUP(void);
void CPT_FT5216_Read_ID_G_THPEAK(void);
void CPT_FT5216_Read_ID_G_THCAL(void);
void CPT_FT5216_Read_ID_G_THWATER(void);
void CPT_FT5216_Read_ID_G_THDIFF(void);
void CPT_FT5216_Read_ID_G_CTRL(void);
void CPT_FT5216_Read_ID_G_TIME_ENTER_MONITOR(void);
void CPT_FT5216_Read_ID_G_PERIOD_ACTIVE(void);
void CPT_FT5216_Read_ID_G_PERIOD_MONITOR(void);
void CPT_FT5216_Read_ID_G_AUTO_CLB_MODE(void);
void CPT_FT5216_Read_ID_G_LIB_VERSION(void);
void CPT_FT5216_Read_ID_G_VENDOR_ID(void);
void CPT_FT5216_Read_ID_G_MODE(void);
void CPT_FT5216_Read_ID_G_PMODE(void);
void CPT_FT5216_Read_ID_G_FIRMID(void);
void CPT_FT5216_Read_ID_G_STATE(void);
void CPT_FT5216_Read_ID_G_FT5201ID(void);
void CPT_FT5216_Read_ID_G_ERR(void);
void CPT_FT5216_Read_ID_G_CLB(void);
void CPT_FT5216_Read_ID_G_B_AREA_TH(void);
void CPT_FT5216_ReadIDGPeriodMonitor(void);
void CPT_FT5216_ReadIDGCLB(void);



void CPT_FT5216_Set_DeviceMode(uint8_t value);
void CPT_FT5216_Set_ID_G_THGROUP(uint8_t value);
void CPT_FT5216_Set_ID_G_THPEAK(uint8_t value);
void CPT_FT5216_Set_ID_G_THCAL(uint8_t value);
void CPT_FT5216_Set_ID_G_THWATER(uint8_t value);
void CPT_FT5216_Set_ID_G_THTEMP(uint8_t value);
void CPT_FT5216_Set_ID_G_THDIFF(uint8_t value);
void CPT_FT5216_Set_ID_G_CTRL(uint8_t value);
void CPT_FT5216_Set_ID_G_TIME_ENTER_MONITOR(uint8_t value);
void CPT_FT5216_Set_ID_G_PERIOD_ACTIVE(uint8_t value);
void CPT_FT5216_Set_ID_G_PERIOD_MONITOR(uint8_t value);
void CPT_FT5216_Set_ID_G_AUTO_CLB_MODE(uint8_t value);
void CPT_FT5216_Set_ID_G_CLB(uint8_t value);


void CPT_FT5216_ReadTouch(void);
uint16_t CPT_FT526_GetCalibY(void);
uint16_t CPT_FT526_GetCalibX(void);



void CPT_FT5216_Config_Defaults(uint8_t value);
void CPT_FT5216_SetCalib(uint16_t x, uint16_t y);
void CPT_FT5216_SetPointCalib(uint16_t x, uint16_t y);



void CPT_FT5216_Init(void);


#endif
 

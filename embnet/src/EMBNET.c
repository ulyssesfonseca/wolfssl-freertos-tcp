/*----------------------------------------------------------------------------
 Embtech Tecnologia Embarcada S/A

 Protocolo EmbNET

 *----------------------------------------------------------------------------*/

#include "EMBNET.h"
#include <stdio.h>
#include "string.h"

#define SER_DADOS	0

//=================================================================================================
//	FRAMES SUPORTADOS PELA EMBNET
//=================================================================================================
uint32_t FRAME_SET        = 0x00010000;
uint32_t FRAME_GET        = 0x00020000;
uint32_t FRAME_REPLY      = 0x00030000;
uint32_t FRAME_GETERROR   = 0x00040000;
uint32_t FRAME_REPLYERROR = 0x00050000;
uint32_t FRAME_ATTRIBERROR= 0x00060000;
uint32_t FRAME_TRIGGER    = 0x00070000;
uint32_t FRAME_SETDEVICE  = 0x000A0000;
uint32_t FRAME_DISCOVERY  = 0x000EFFFF; 
uint32_t FRAME_LIVE       = 0x000FFFFF;




//=================================================================================================
//	VARIAVEIS PRIVADAS DA EMBNET
//=================================================================================================
//obs.:Estes parametros nao devem ser externados
uint32_t _DeviceID 				= 0x00; 				//Identifica o endere�o do device na rede
uint32_t _NetID 				= 0x00;					//Identifica o endereco do device
uint16_t _DeviceClass  			= 0x0000; 				//determina a classe do dispositivo
uint32_t _DeviceSerialNumber  	= 0x0000000000;			//Serial number do device
uint8_t  _DeviceType			= 0x00; 				//Tipo do dispositivo EMBNET 0-Slave only 1-Master Only  2-Master/Slave


uint8_t  _DeviceStarted=0; 			//Flag que sinaliza que o dispositivo foi inicializado 1=Inicializado
uint8_t  _DeviceOFFLine=1; 			//flag que sinaliza que o dispositivo esta off line ainda n�o inicializado valido somente quando slave
uint16_t _TimeOutOFFLine=40; 		//Tempo que o device leva para cair para off line caso o master deixe de resetar o tempo de refresh live
uint8_t  _CountLive=1; 				//contator de 1 segundos de envio de live quando offline
uint16_t _CountTimeOutOFFLine=0; 	//contador para cair no estado offline


uint16_t _CountSetDevice=0;			//Flag que libera por um tempo para setar parametros do Device








//=================================================================================================
//
//=================================================================================================
/*************************************************************************************************
 * Função utilizada para tratar os frames recebidos
 *
 * esta função foi criada para padronizar a recepção de diferentes microcontroladores
 *
 *************************************************************************************************/
void __EmbNet_ReadFrames(CAN_msg CAN_Rx){
	volatile uint16_t tmpAttribute;
	volatile uint32_t tmpData;
	_EmbNET_Sniffer(CAN_RxMsg);
	//Pacotes broadcast
	if (CAN_Rx.id == FRAME_DISCOVERY) { EmbNET_SendLive();  } 	//recebe um pacote discovery

	if (CAN_Rx.id == FRAME_LIVE) {  														//----Recebe um pacote tipo live-----------

		tmpAttribute = (uint16_t)CAN_Rx.data[2];
		tmpAttribute = tmpAttribute  << 8;
		tmpAttribute |= (uint16_t)CAN_Rx.data[3];

		tmpData = (uint32_t)CAN_Rx.data[7];
		tmpData |= ((uint32_t)CAN_Rx.data[6]) << 8;
		tmpData |= ((uint32_t)CAN_Rx.data[5]) << 16;
		tmpData |= ((uint32_t)CAN_Rx.data[4]) << 24;

		_EmbNET_CallBk_Live(CAN_Rx.data[0], CAN_Rx.data[1], tmpAttribute, tmpData);
	}

	if((CAN_RxMsg.id&0x000F0000) == FRAME_SETDEVICE){
		tmpAttribute = (uint16_t)CAN_Rx.data[2];
		tmpAttribute = tmpAttribute  << 8;
		tmpAttribute |= (uint16_t)CAN_Rx.data[3];

		tmpData = (uint32_t)CAN_Rx.data[7];
		tmpData |= ((uint32_t)CAN_Rx.data[6]) << 8;
		tmpData |= ((uint32_t)CAN_Rx.data[5]) << 16;
		tmpData |= ((uint32_t)CAN_Rx.data[4]) << 24;
		_EmbNET_CallBk_FrameSetDevice((uint8_t)(CAN_Rx.id&0x000000FF),(uint8_t)((CAN_Rx.id & 0x0000FF00)>>8),CAN_Rx.data[0], CAN_Rx.data[1], tmpAttribute, tmpData);
	}

	//Pacotes destinados ao device local
	if (((CAN_Rx.id & 0x000000FF) == (_DeviceID)) && ((CAN_Rx.id & 0x0000FF00) == (_NetID)) && ((!_DeviceOFFLine) || ((CAN_Rx.data[2]<<8|CAN_RxMsg.data[3])==0x0101)))  {

		if 			((CAN_Rx.id & 0x000F0000) == FRAME_SET) { 					//recebe um pacote tipo SET
			tmpAttribute = (uint16_t)CAN_Rx.data[2];
			tmpAttribute = tmpAttribute  << 8;
			tmpAttribute |= (uint16_t)CAN_Rx.data[3];

			tmpData = (uint32_t)CAN_Rx.data[7];
			tmpData |= ((uint32_t)CAN_Rx.data[6]) << 8;
			tmpData |= ((uint32_t)CAN_Rx.data[5]) << 16;
			tmpData |= ((uint32_t)CAN_Rx.data[4]) << 24;

			_EmbNET_CallBk_FrameSet( CAN_Rx.data[0], CAN_Rx.data[1], tmpAttribute, tmpData);
		}
		else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_GET) { 					//Recebe um pacote tipo GET

			tmpAttribute = (uint16_t)CAN_Rx.data[2];
			tmpAttribute = tmpAttribute  << 8;
			tmpAttribute |= (uint16_t)CAN_Rx.data[3];

			tmpData = (uint32_t)CAN_Rx.data[7];
			tmpData |= ((uint32_t)CAN_Rx.data[6]) << 8;
			tmpData |= ((uint32_t)CAN_Rx.data[5]) << 16;
			tmpData |= ((uint32_t)CAN_Rx.data[4]) << 24;

			//              _EmbNET_CallBk_FrameGet( CAN_RxMsg.data[0], CAN_RxMsg.data[1], tmpAttribute);
			_EmbNET_CallBk_FrameGet(CAN_Rx.data[0],CAN_Rx.data[1],tmpAttribute,tmpData);
		}
		else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_REPLY) { 				//recebe um pacote tipo Reply

			tmpAttribute = (uint16_t)CAN_Rx.data[2];
			tmpAttribute = tmpAttribute  << 8;
			tmpAttribute |= (uint16_t)CAN_Rx.data[3];

			tmpData = (uint32_t)CAN_Rx.data[7];
			tmpData |= ((uint32_t)CAN_Rx.data[6]) << 8;
			tmpData |= ((uint32_t)CAN_Rx.data[5]) << 16;
			tmpData |= ((uint32_t)CAN_Rx.data[4]) << 24;

			_EmbNET_CallBk_FrameReply(CAN_Rx.data[0], CAN_Rx.data[1], tmpAttribute, tmpData);
		}
		else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_TRIGGER) {

			tmpAttribute = (uint16_t)CAN_Rx.data[2];
			tmpAttribute = tmpAttribute  << 8;
			tmpAttribute |= (uint16_t)CAN_Rx.data[3];

			tmpData = (uint32_t)CAN_Rx.data[7];
			tmpData |= ((uint32_t)CAN_Rx.data[6]) << 8;
			tmpData |= ((uint32_t)CAN_Rx.data[5]) << 16;
			tmpData |= ((uint32_t)CAN_Rx.data[4]) << 24;

			_EmbNET_CallBk_FrameTrigger(CAN_Rx.data[0], CAN_Rx.data[1], tmpAttribute, tmpData);
		}
		else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_ATTRIBERROR) {

			tmpAttribute = (uint16_t)CAN_Rx.data[2];
			tmpAttribute = tmpAttribute  << 8;
			tmpAttribute |= (uint16_t)CAN_Rx.data[3];


			_EmbNET_CallBk_FrameAttribError(CAN_Rx.data[0], CAN_Rx.data[1], tmpAttribute);
		}
		else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_GETERROR) {

			tmpAttribute = (uint16_t)CAN_Rx.data[2];
			tmpAttribute = tmpAttribute  << 8;
			tmpAttribute |= (uint16_t)CAN_Rx.data[3];


			_EmbNET_CallBk_FrameGetError(CAN_Rx.data[0], CAN_Rx.data[1], tmpAttribute);
		}
		else if ((CAN_RxMsg.id & 0x000F0000) == FRAME_REPLYERROR) {

			tmpAttribute =  (uint16_t)CAN_Rx.data[2];
			tmpAttribute =  tmpAttribute  << 8;
			tmpAttribute |= (uint16_t)CAN_Rx.data[3];

			tmpData =  (uint32_t)CAN_Rx.data[7];
			tmpData |= ((uint32_t)CAN_Rx.data[6]) << 8;
			tmpData |= ((uint32_t)CAN_Rx.data[5]) << 16;
			tmpData |= ((uint32_t)CAN_Rx.data[4]) << 24;

			_EmbNET_CallBk_FrameReplyError(CAN_Rx.data[0], CAN_Rx.data[1], tmpAttribute, tmpData);
		}

	}
}






//=================================================================================================
//	FUNCOES DE CONFIGURACAO E STARTUP DA EMBNET
//=================================================================================================
/******************************************************************************
	EmbNET_StartUp

	Fun��o criada para configurar os parametros de CAN,
	definir os filtros na rede, e iniciar a comunica��o com a rede CAN.
 ******************************************************************************/
void EmbNET_StartUp(void) {

	CAN_setup ();                                   /* setup CAN Controller   */


	//Define os filtros para os pacotes EmbNet
	CAN_wrFilter (FRAME_SETDEVICE, EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_DISCOVERY, EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_LIVE, EXTENDED_FORMAT);

#if (SNIFFER==1)	//remove os filtros de id e net quando em modo Sniffer
	CAN_wrFilter (FRAME_SET , EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_GET , EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_GETERROR , EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_ATTRIBERROR , EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_REPLY , EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_REPLYERROR , EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_TRIGGER , EXTENDED_FORMAT);
#else
	CAN_wrFilter ((FRAME_SET | _NetID | _DeviceID) , EXTENDED_FORMAT);
	CAN_wrFilter ((FRAME_GET | _NetID | _DeviceID) , EXTENDED_FORMAT);
	CAN_wrFilter ((FRAME_GETERROR | _NetID | _DeviceID) , EXTENDED_FORMAT);
	CAN_wrFilter ((FRAME_ATTRIBERROR | _NetID | _DeviceID) , EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_REPLYERROR | _NetID | _DeviceID, EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_REPLY | _NetID | _DeviceID, EXTENDED_FORMAT);
	CAN_wrFilter (FRAME_TRIGGER | _NetID | _DeviceID, EXTENDED_FORMAT);
#endif


	CAN_start ();                                 
	CAN_waitReady ();  



	if (_DeviceType == EMBNET_SLAVE) {
		EmbNET_SendLive();
	}
	_DeviceOFFLine=1;
	_DeviceStarted=1;

}
/******************************************************************************
	EmbNET_SetDevice

	Esta fun��o � uma fun��o interna do m�dulo,
	que atribui a identifica��o individual e de rede, o tipo de modulo, 
	e a classe.

	Dever� ser usada no startup do m�dulo, e toda vez que for necess�rio alterar,
	alguma identifica��o deste.

	@param
		sDeviceID - identifica��o do modulo na rede
		sNetID	- rede que o m�dulo ir� trabalhar
		Mode	- se o m�dulo ser� master, slave ou ambos
		Class	- identifica o m�dulo de acordo com as caracteristicas unicas deste
		sSerial1 - lote do modulo
		sSerial2 - s�rie do modulo
 ******************************************************************************/
void EmbNET_SetDevice(uint8_t sDeviceID, uint8_t sNetID, uint8_t Mode, uint16_t Class, uint16_t sSerial1, uint16_t sSerial2) {

	_DeviceID = 0x000000FF & (uint32_t)sDeviceID;
	_NetID    = 0x0000FF00 & ((uint32_t)sNetID << 8);
	if(Mode==0){_DeviceType=EMBNET_SLAVE;}
	if(Mode==1){_DeviceType=EMBNET_MASTER;}
	if(Mode==2){_DeviceType=EMBNET_MASTER_SLAVE;}
	_DeviceClass = Class;

	_DeviceClass = Class;
	_DeviceSerialNumber = (uint32_t) (sSerial1 << 16) | (uint32_t)sSerial2;
}






//=================================================================================================
//	FUNCOES DE SEND DA EMBNET
//=================================================================================================
/******************************************************************************
	Este frame gerado pelo slave com destino broadcasting �onde todos devices 
	recebem� cont�m informa��es de um determinado slave. Esse frame sinaliza a
	exist�ncia de um slave na rede que ainda n�o foi inicializado por um Master 
	e est� pronto para operar. Esse pacote � gerado a cada 1 segundo antes da 
	inicializa��o e depois que essa ocorre ele � gerado somente com a requisi��o 
	atrav�s de um Frame DISCOVERY na rede.
 ******************************************************************************/
void EmbNET_SendLive(void) {

	CAN_TxMsg.id     = FRAME_LIVE;
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & _DeviceClass;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(_DeviceClass >> 8);
	CAN_TxMsg.data[4] =  0x000000FF & 	_DeviceSerialNumber;
	CAN_TxMsg.data[7] =  0x000000FF &  (_DeviceSerialNumber >> 8);
	CAN_TxMsg.data[6] =  0x000000FF &  (_DeviceSerialNumber >> 16);
	CAN_TxMsg.data[5] =  0x000000FF &  (_DeviceSerialNumber >> 24);

	CAN_wrMsg(&CAN_TxMsg);

//	Board_UARTPutSTR("Live\n\r");
#if SER_DADOS == 1
	printf("SendFrame_Live-");
	printf("DeviceID= %u -",DeviceID);
	printf("NetID= %u;\r\n",NetID);
#endif
}
/******************************************************************************
	Este frame permite setar um atributo que aceita escrita enviando um novo 
	valor. Enviado pelo Master a um Slave.

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
		AtributeValue	- o valor a ser passado
 ******************************************************************************/
void EmbNET_SendFrame_Set(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue) {

	CAN_TxMsg.id     = FRAME_SET | ToDeviceID | (ToNetID << 8);
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & AttributeID;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(AttributeID >> 8);
	CAN_TxMsg.data[7] =  0x000000FF & AtributeValue;
	CAN_TxMsg.data[6] =  0x000000FF &  (AtributeValue >> 8);
	CAN_TxMsg.data[5] =  0x000000FF &  (AtributeValue >> 16);
	CAN_TxMsg.data[4] =  0x000000FF &  (AtributeValue >> 24);


	CAN_wrMsg(&CAN_TxMsg);

#if SER_DADOS == 1
	printf("SendFrame_Set-");
	printf("ToDeviceID= %u -",ToDeviceID);
	printf("ToNetID= %u -",ToNetID);
	printf("AtributeID= %u -",AtributeID);
	printf("AtributeValue= %u;\r\n",AtributeValue);
#endif

}
/******************************************************************************
	Este frame solicita a leitura de um determinado atributo que seja 
	leitura/escrita ou somente leitura. Enviado pelo Master a um Slave.

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
		AtributeValue - parametro que define qual valor que ser� lido
 ******************************************************************************/
void EmbNET_SendFrame_Get(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue) {

	CAN_TxMsg.id     = FRAME_GET | ToDeviceID | (ToNetID << 8);
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & AttributeID;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(AttributeID >> 8);
	CAN_TxMsg.data[7] =  0x000000FF & AtributeValue;
	CAN_TxMsg.data[6] =  0x000000FF &  (AtributeValue >> 8);
	CAN_TxMsg.data[5] =  0x000000FF &  (AtributeValue >> 16);
	CAN_TxMsg.data[4] =  0x000000FF &  (AtributeValue >> 24);

	//	CAN_TxMsg.data[7] =  0x00;
	//	CAN_TxMsg.data[6] =  0x00;
	//	CAN_TxMsg.data[5] =  0x00;
	//	CAN_TxMsg.data[4] =  0x00;

	CAN_wrMsg(&CAN_TxMsg);

#if SER_DADOS == 1
	printf("SendFrame_Get-");
	printf("ToDeviceID= %u -",ToDeviceID);
	printf("ToNetID= %u -",ToNetID);
	printf("AtributeID= %u;\r\n",AtributeID);
#endif
}
/******************************************************************************
	Este frame � utilizado quando uma a��o requer um retorno imediato, 
	sem necessitar de um frame GET.

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
		AtributeValue	- o valor a ser passado
 ******************************************************************************/
void EmbNET_SendFrame_Trigger(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue){

	CAN_TxMsg.id     = FRAME_TRIGGER | ToDeviceID | (ToNetID << 8);
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & AttributeID;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(AttributeID >> 8);
	CAN_TxMsg.data[7] =  0x000000FF & AtributeValue;
	CAN_TxMsg.data[6] =  0x000000FF &  (AtributeValue >> 8);
	CAN_TxMsg.data[5] =  0x000000FF &  (AtributeValue >> 16);
	CAN_TxMsg.data[4] =  0x000000FF &  (AtributeValue >> 24);


	CAN_wrMsg(&CAN_TxMsg);

#if SER_DADOS == 1
	printf("SendFrame_Trigger-");
	printf("ToDeviceID= %u -",ToDeviceID);
	printf("ToNetID= %u -",ToNetID);
	printf("AtributeID= %u -",AtributeID);
	printf("AtributeValue= %u;\r\n",AtributeValue);
#endif

}
/******************************************************************************
	Este frame � uma resposta ao frame GET com dados do atributo espec�fico. 
	Este frame � enviado pelo Slave com destino ao Master solicitante que 
	originou a requisi��o com o frame GET.

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
		AtributeValue	- o valor a ser passado
 ******************************************************************************/
void EmbNET_SendFrame_Reply(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue) {

	CAN_TxMsg.id     = FRAME_REPLY | ToDeviceID | (ToNetID << 8);
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & AttributeID;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(AttributeID >> 8);
	CAN_TxMsg.data[7] =  0x000000FF & AtributeValue;
	CAN_TxMsg.data[6] =  0x000000FF &  (AtributeValue >> 8);
	CAN_TxMsg.data[5] =  0x000000FF &  (AtributeValue >> 16);
	CAN_TxMsg.data[4] =  0x000000FF &  (AtributeValue >> 24);


	CAN_wrMsg(&CAN_TxMsg);

#if SER_DADOS == 1
	printf("SendFrame_Reply-");
	printf("ToDeviceID= %u -",ToDeviceID);
	printf("ToNetID= %u -",ToNetID);
	printf("AtributeID= %u -",AtributeID);
	printf("AtributeValue= %u;\r\n",AtributeValue);
#endif

}
/******************************************************************************
	Este frame � gerado toda vez que um atributo n�o � reconhecido pelo m�dulo, 
	gerando um erro neste mesmo atributo.

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
 ******************************************************************************/
void EmbNET_SendFrame_AttribError(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID) {

	CAN_TxMsg.id     = FRAME_ATTRIBERROR| ToDeviceID | (ToNetID << 8);
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & AttributeID;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(AttributeID >> 8);
	CAN_TxMsg.data[7] =  0x00;
	CAN_TxMsg.data[6] =  0x00;
	CAN_TxMsg.data[5] =  0x00;
	CAN_TxMsg.data[4] =  0x00;

	CAN_wrMsg(&CAN_TxMsg);

#if SER_DADOS == 1
	printf("SendFrame_AttibError-");
	printf("ToDeviceID= %u -",ToDeviceID);
	printf("ToNetID= %u -",ToNetID);
	printf("AtributeID= %u;\r\n",AtributeID);
#endif

}
/******************************************************************************
	Este frame utilizado na etapa de configura��o de um iModule, normalmente s�
	pode ser utilizado quando tem um �nico iModule na rede para permitir setar o
	endere�o �nico do dispositivo de forma direta sem descoberta do endere�o
	anterior. Normalmente esse frame � utilizado apenas durante a configura��o
	de f�brica de um iModule ou atrav�s do HandHeld EmbNET para alterar o
	endere�o padr�o de um iModule.

	Esta fun��o deve primeiro receber o frame de libera��o da flag,
	para ativar a escrita em fun��es de configura��o.

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
		AtributeValue	- o valor a ser passado	
 ******************************************************************************/
void EmbNET_SendFrame_SetDevice(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue){

	CAN_TxMsg.id     = FRAME_SETDEVICE | ToDeviceID | (ToNetID << 8);
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & AttributeID;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(AttributeID >> 8);
	CAN_TxMsg.data[7] =  0x000000FF & AtributeValue;
	CAN_TxMsg.data[6] =  0x000000FF &  (AtributeValue >> 8);
	CAN_TxMsg.data[5] =  0x000000FF &  (AtributeValue >> 16);
	CAN_TxMsg.data[4] =  0x000000FF &  (AtributeValue >> 24);


	CAN_wrMsg(&CAN_TxMsg);

#if SER_DADOS == 1
	printf("SendFrame_SetDevice-");
	printf("ToDeviceID= %u -",ToDeviceID);
	printf("ToNetID= %u -",ToNetID);
	printf("AtributeID= %u -",AtributeID);
	printf("AtributeValue= %u;\r\n",AtributeValue);
#endif

}
/******************************************************************************
	Solicita que todos os devices conectados, retornem um LIVE.
 ******************************************************************************/
void EmbNET_SendFrame_Discovery(void){
	CAN_TxMsg.id     = FRAME_DISCOVERY;
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  0x00;
	CAN_TxMsg.data[2] =  0x00;
	CAN_TxMsg.data[7] =  0x00;
	CAN_TxMsg.data[6] =  0x00;
	CAN_TxMsg.data[5] =  0x00;
	CAN_TxMsg.data[4] =  0x00;


	CAN_wrMsg(&CAN_TxMsg);

#if SER_DADOS == 1
	printf("SendFrame_SetDevice-");
	printf("ToDeviceID= %u -",ToDeviceID);
	printf("ToNetID= %u -",ToNetID);
	printf("AtributeID= %u -",AtributeID);
	printf("AtributeValue= %u;\r\n",AtributeValue);
#endif
}
/******************************************************************************
	Este frame � gerado por um device quando solicitado pelo frame GETERROR 
	com detalhes de error de comunica��o do iModule em uma rede EmbNET.	

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
		AtributeValue	- o valor a ser passado	
 ******************************************************************************/
void EmbNET_SendFrame_ReplyError(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue){

	CAN_TxMsg.id     = FRAME_REPLYERROR | ToDeviceID | (ToNetID << 8);
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & AttributeID;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(AttributeID >> 8);
	CAN_TxMsg.data[7] =  0x000000FF & AtributeValue;
	CAN_TxMsg.data[6] =  0x000000FF &  (AtributeValue >> 8);
	CAN_TxMsg.data[5] =  0x000000FF &  (AtributeValue >> 16);
	CAN_TxMsg.data[4] =  0x000000FF &  (AtributeValue >> 24);


	CAN_wrMsg(&CAN_TxMsg);

}
/******************************************************************************
	Este frame � gerado por um device quando solicitado pelo frame GETERROR 
	com detalhes de error de comunica��o do iModule em uma rede EmbNET.	

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
 ******************************************************************************/
void EmbNET_SendFrame_GetError(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID){

	CAN_TxMsg.id     = FRAME_GETERROR | ToDeviceID | (ToNetID << 8);
	CAN_TxMsg.len    = 8;
	CAN_TxMsg.format = EXTENDED_FORMAT;
	CAN_TxMsg.type   = DATA_FRAME;
	CAN_TxMsg.data[0] =  (unsigned char) _DeviceID;
	CAN_TxMsg.data[1] =  (unsigned char)(_NetID >> 8);
	CAN_TxMsg.data[3] =  (unsigned char) 0x00FF & AttributeID;
	CAN_TxMsg.data[2] =  (unsigned char) 0x00FF &(AttributeID >> 8);
	CAN_TxMsg.data[7] =  0x00;
	CAN_TxMsg.data[6] =  0x00;
	CAN_TxMsg.data[5] =  0x00;
	CAN_TxMsg.data[4] =  0x00;


	CAN_wrMsg(&CAN_TxMsg);
}





//=================================================================================================
//	FUNCOES DE CALLBK DA EMBNET
//=================================================================================================
/******************************************************************************
	Fun��o de chamada para toda vez que o m�dulo for inicializado por um master.
 ******************************************************************************/
void _EmbNET_CallBk_OnLineDevice(void) {

#if CALLBK_ONLINE == 1
	EmbNET_CallBk_ONLine(); //chama rotina de de callback do usu�rio
#endif

}
/******************************************************************************
	Fun��o de chamada para toda vez que o m�dulo entrar no estado de off-line.
 ******************************************************************************/
void _EmbNET_CallBk_OFFLineDevice(void) {

#if CALLBK_OFFLINE == 1
	EmbNET_CallBk_OFFLine(); //chama rotina de de callback do usu�rio
#endif

}
/******************************************************************************
	Chamada sempre que � recebido um frame de LIVE

	@param
		FDeviceID - Device que enviou o Frame
		FNetID	- de qual rede foi enviado
		Class	- Classe do m�dulo que enviou
		Serial	- numero de s�rie
 ******************************************************************************/
void _EmbNET_CallBk_Live( uint8_t FDeviceID, uint8_t FNetID, uint16_t Class, uint32_t Serial ) {

#if CALLBK_LIVE == 1
	EmbNET_CallBk_Live(FDeviceID,FNetID,Class,Serial);
#endif
}
/******************************************************************************
	Chamada para solicita��o de um GET.

	@param
		FDeviceID - Device que enviou o Frame
		FNetID	- de qual rede foi enviado
		AttributeID - Atributo solicitado
		DataReply - ponteiro que dever� receber o valor para passar para o reply.
 ******************************************************************************/
void _EmbNET_CallBk_FrameGet( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data) {

#if CALLBK_FRAMEGET == 1
	uint32_t DataReply=0x00000000;
	uint8_t  ValidReply=0x00;
#endif

#if SER_DADOS == 1
	printf("CallBackFrame_Get-");
	printf("FDeviceID= %u -",FDeviceID);
	printf("FNetID= %u -",FNetID);
	if(AttributeID <0x1000){printf("AttributeID= %u;\r\n",AttributeID);}
	else{
		printf("AttributeID= %u -",AttributeID);
		printf("DataReply= %u;\r\n",DataReply);
	}

#endif

	switch(AttributeID) {

	case Attrib_Get_Version : 			{ EmbNET_SendFrame_Reply(FDeviceID,FNetID,AttributeID,(uint32_t)EmbNET_VERSION);  } break; 			//get vers�o do protocolo EmbNET Suportado por este modulo
	case Attrib_Set_Time_Out_Slave : 	{ EmbNET_SendFrame_Reply(FDeviceID,FNetID,AttributeID,_TimeOutOFFLine);  } break; 							//consulta a variavel TimeOutOFFLine
	case Attrib_DeviceID_NetID : 		{ EmbNET_SendFrame_Reply(FDeviceID,FNetID,AttributeID,(uint32_t)(_DeviceID | _NetID)); } break;	//get Device ID e NetID do module
	case Attrib_Serial : 				{ EmbNET_SendFrame_Reply(FDeviceID,FNetID,AttributeID,(uint32_t)_DeviceSerialNumber);} break;		//consulta o numero serial do equipamento
	case Attrib_Get_ClassId : 			{ EmbNET_SendFrame_Reply(FDeviceID,FNetID,AttributeID,(uint32_t)_DeviceClass); } break; 				//get ClassID do modulo
	default : {
#if CALLBK_FRAMEGET == 1
		//EmbNET_CallBk_FrameGet(FDeviceID,FNetID,AttributeID,&DataReply); //chama rotina de de callback do usu�rio
		EmbNET_CallBk_FrameGet(FDeviceID,FNetID,AttributeID,&DataReply,Data, &ValidReply);
		if(!ValidReply){
			EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);
		}
		else{
			EmbNET_SendFrame_Reply(FDeviceID,FNetID,AttributeID,DataReply); //responde um pacote reply a chamada Get *mandat�rio
		}
#else
		EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);
#endif
	}
	}
}
/******************************************************************************
	Chamada para um frame SET.

	@param
		FDeviceID - Device que enviou o Frame
		FNetID	- de qual rede foi enviado
		AttributeID - Atributo solicitado
		Data - Valor recebido pelo frame SET
 ******************************************************************************/
void _EmbNET_CallBk_FrameSet( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data ) {

#if SER_DADOS == 1
	printf("CallBackFrame_Set-");
	printf("FDeviceID= %u -",FDeviceID);
	printf("FNetID= %u -",FNetID);
	printf("AttributeID= %u -",AttributeID);
	printf("Data= %u;\r\n",Data);
#endif

	switch(AttributeID)  {

	case Attrib_Set_Init_Slave: 	{ _DeviceOFFLine=0; _CountTimeOutOFFLine=1; _EmbNET_CallBk_OnLineDevice();  } break; //inicializa o slave
	case Attrib_Set_Turn_Off_Slave: { _DeviceOFFLine=1; _EmbNET_CallBk_OFFLineDevice(); } break; //para o slave torna o mesmo offline
	case Attrib_Set_Reset_TimeOut: 	{ _CountTimeOutOFFLine=1; } break; //reset timeout auto offline do slave
	case Attrib_Set_Time_Out_Slave: { _TimeOutOFFLine=(uint16_t)Data;  } break; //set o tempo em seg do timeout auto offline do slave
	case Attrib_EmbNet_Senha: {
		/*
				Se receber a senha 000597, libera recursos conforme o tempo dos ultimos 2 bytes, por no m�ximo 60 segundos.
		 */
		if(((Data&0x00F00000)>>20)==0 && ((Data&0x000F0000)>>16)==0 && ((Data&0x0000F000)>>12)==0 &&
				((Data&0x00000F00)>>8 )==5 && ((Data&0x000000F0)>>4 )==9 && ((Data&0x0000000F))==7){
			_CountSetDevice=(((Data&0xF0000000)>>24) | ((Data&0x0F000000)>>24))*10;
			if(_CountSetDevice>=600){_CountSetDevice=600;}
		}
	} break;
	case Attrib_EmbNet_Erro_Reset: {
#if CALLBK_FRAMESET == 1
		if(_CountSetDevice>0){EmbNET_CallBk_FrameSet(FDeviceID,FNetID,AttributeID,Data);}
		else{EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);}
#else
		EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);
#endif
	} break;
	case Attrib_Teste:{			//(w/r)Realiza a escrita e leitura do registrador de teste
#if CALLBK_FRAMESET == 1
		if(_CountSetDevice>0){EmbNET_CallBk_FrameSet(FDeviceID, FNetID, AttributeID, Data);}
		else{EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);}
#else
		EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);
#endif
	} break;
	default: {

		if ((AttributeID > 0x105) && (AttributeID <= 0x3E8))  { EmbNET_SendFrame_AttribError(FDeviceID,FNetID,AttributeID); }
		else{
#if CALLBK_FRAMESET == 1
			EmbNET_CallBk_FrameSet(FDeviceID,FNetID,AttributeID,Data); //chama rotina de de callback do usu�rio
#else
			EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);
#endif
		}
	}

	}
}
/******************************************************************************
	Retorno autom�tico quando um evento pr� determinado � ativado.

	@param
		FDeviceID - Device que enviou o Frame
		FNetID	- de qual rede foi enviado
		AttributeID - Atributo solicitado
		Data - Valor recebido	
 ******************************************************************************/
void _EmbNET_CallBk_FrameTrigger( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data ) {

#if CALLBK_FRAMETRIGGER == 1
	EmbNET_CallBk_FrameTrigger(FDeviceID,FNetID,AttributeID,Data);

#if SER_DADOS == 1
	printf("CallBackFrame_Trigger-");
	printf("FDeviceID= %u -",FDeviceID);
	printf("FNetID= %u -",FNetID);
	printf("AttributeID= %u -",AttributeID);
	printf("Data= %u;\r\n",Data);
#endif

#endif

}
/******************************************************************************
	Retorno para o frame GET.

	@param
		FDeviceID - Device que enviou o Frame
		FNetID	- de qual rede foi enviado
		AttributeID - Atributo solicitado
		Data - Valor recebido	
 ******************************************************************************/
void _EmbNET_CallBk_FrameReply( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data ) {

#if CALLBK_FRAMEREPLY == 1
	EmbNET_CallBk_FrameReply(FDeviceID,FNetID,AttributeID,Data);

#if SER_DADOS == 1
	printf("CallBackFrame_Reply-");
	printf("FDeviceID= %u -",FDeviceID);
	printf("FNetID= %u -",FNetID);
	printf("AttributeID= %u -",AttributeID);
	printf("Data= %u;\r\n",Data);
#endif

#endif

}
/******************************************************************************
	Este frame utilizado na etapa de configura��o de um iModule, normalmente s�
	pode ser utilizado quando tem um �nico iModule na rede para permitir setar o
	endere�o �nico do dispositivo de forma direta sem descoberta do endere�o
	anterior. Normalmente esse frame � utilizado apenas durante a configura��o
	de f�brica de um iModule ou atrav�s do HandHeld EmbNET para alterar o
	endere�o padr�o de um iModule.

	Esta fun��o deve primeiro receber o frame de libera��o da flag,
	para ativar a escrita em fun��es de configura��o.

	@param
		ToDeviceID - qual device deve receber este frame
		ToNetID	- em qual rede se encontra
		AtributeID	- o atributo que ir� identificar
		AtributeValue	- o valor a ser passado	
 ******************************************************************************/
void _EmbNET_CallBk_FrameSetDevice( uint8_t ToDeviceID, uint8_t ToNetID, uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data){

	switch(AttributeID){

	case 0x0120:{
		if(Data==_DeviceSerialNumber){
			_DeviceID = 0x000000FF & (uint32_t)ToDeviceID;
			_NetID    = 0x0000FF00 & ((uint32_t)ToNetID << 8);
			EmbNET_StartUp();
			_DeviceOFFLine=0;
		}
		else{EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);}
	} break;	//Altera o endere�o do dispositivo
	case 0x0121:{//Configura o numero Serial do dispositivo
		if(_DeviceID==ToDeviceID && _NetID==(ToNetID<<8)){
#if CALLBK_FRAMESETDEVICE == 1
			if(_CountSetDevice>0){EmbNET_CallBk_FrameSetDevice(FDeviceID, FNetID, AttributeID, Data); _DeviceOFFLine=0;}
			else{EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);}
#else
			EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);
#endif
		}
	} break;
	default: {
		if(_DeviceID==ToDeviceID && _NetID==(ToNetID<<8)){
#if CALLBK_FRAMESETDEVICE == 1
			EmbNET_CallBk_FrameSetDevice(FDeviceID, FNetID, AttributeID, Data);
#else
			EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);
#endif
		}
	} break;
	}
}
/******************************************************************************
	Chamada sempre que um atributo � inexistente no device solicitado.

	@param
		FDeviceID - Device que enviou o Frame
		FNetID	- de qual rede foi enviado
		Class	- Classe do m�dulo que enviou
 ******************************************************************************/
void _EmbNET_CallBk_FrameAttribError(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID){
#if CALLBK_FRAMEATTRIBERROR == 1
	EmbNET_CallBk_FrameAttribError(FDeviceID, FNetID, AttributeID);
#endif
}
/******************************************************************************
	Chamada para retornar os erros de recep��o e transmiss�o do m�dulo pela CAN.

	@param
		ToDeviceID - Device que solicitou a leitura de erros
		ToNetID	- de qual rede foi enviado
		AtributeID	- Erros solicitados
 ******************************************************************************/
void _EmbNET_CallBk_FrameGetError(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID){

	switch(AttributeID){
	case 0x0401: { EmbNET_SendFrame_ReplyError(FDeviceID,FNetID,AttributeID,(uint32_t)CAN_Error()); } break;
	default:{
#if CALLBK_FRAMEGETERROR == 1
		EmbNET_CallBk_FrameGetError(FDeviceID, FNetID, AttributeID);
#else
		EmbNET_SendFrame_AttribError(FDeviceID, FNetID, AttributeID);
#endif
	} break;
	}

}
/******************************************************************************
	Chamada sempre que um atributo � inexistente no device solicitado.

	@param
		FDeviceID - Device que enviou o Frame
		FNetID	- de qual rede foi enviado
		Class	- Classe do m�dulo que enviou
 ******************************************************************************/
void _EmbNET_CallBk_FrameReplyError(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t AtributeValue){
#if CALLBK_FRAMEGETERROR == 1
	EmbNET_CallBk_FrameReplyError(FDeviceID, FNetID, AttributeID, AtributeValue);
#endif
}






//=================================================================================================
//	FUNCOES DE TIMMING DA EMBNET
//=================================================================================================
/******************************************************************************
	Esta fun��o dever� ser implementada em m�dulos do tipo slave, para
	o envio do frame LIVE e para determinar o tempo que o m�dulo dever�
	cair para off-line.
 ******************************************************************************/
void EmbNET_TickTime100ms(void) {
	if (_DeviceType==0 || _DeviceType==2){
		if (_DeviceStarted) {
			_CountLive++;
			if (_CountLive == 10) { _CountLive=1;  
			if (_DeviceOFFLine) { EmbNET_SendLive();} //se tiver off line envia um pacote Live
			} 
			if (_DeviceOFFLine==0) { _CountTimeOutOFFLine++; }
			if (_CountTimeOutOFFLine >= _TimeOutOFFLine) {
				_DeviceOFFLine=1; //sinaliza que esta offline	
				_EmbNET_CallBk_OFFLineDevice();
			}
		}
	}

	/*
		Contador que ativa recursos por no m�ximo 60 seg.
	 */
	if(_CountSetDevice>0){
		_CountSetDevice--;
	}
}




//=================================================================================================
//	FUNCOES DE SNIFFER DA EMBNET/CAN
//=================================================================================================
/******************************************************************************
	Fun��o para debugar a CAN, independente do protocolo EmbNet

	@return 
		Retorna o tipo CAN_msg
		typedef struct  {
			uint32_t  id;                       // 29 bit identifier                    
			uint8_t   data[8];                  // Data field                           
			uint8_t   len;                      // Length of data field in bytes        
			uint8_t   format;                   // 0 - STANDARD, 1- EXTENDED IDENTIFIER 
			uint8_t   type;                     // 0 - DATA FRAME, 1 - REMOTE FRAME     
		} CAN_msg;
 ******************************************************************************/
void _EmbNET_Sniffer(CAN_msg sMsg){
#if SNIFFER == 1
	EmbNET_Sniffer(sMsg);
#endif
}





#ifndef _EMBNET_H_
#define _EMBNET_H_
#include <stdint.h>                   /* Include standard types               */
#include "EMBNET_CONFIG_USER.h"

#ifdef _CAN_LPC11_
#include "CAN_LPC11.h"
#endif
#ifdef _CAN_LPC1788_
#include "CAN_LPC1788.h"
#endif


//=================================================================================================
//	VERSAO DA EMBNET
//=================================================================================================
#define EmbNET_VERSION 0x01000704




//=================================================================================================
//	TIPOS DE DEVICES DA EMBNET
//=================================================================================================
#define EMBNET_SLAVE 0
#define EMBNET_MASTER 1
#define EMBNET_MASTER_SLAVE 2





//=================================================================================================
//	PROTOTIPOS EXTERNADOS DE FUNCOES DA EMBNET
//=================================================================================================
#if CALLBK_LIVE == 1
extern void EmbNET_CallBk_Live(uint8_t FDeviceID, uint8_t FNetID, uint16_t Class, uint32_t Serial);
#endif

#if CALLBK_ONLINE == 1
extern void EmbNET_CallBk_ONLine(void);
#endif

#if CALLBK_OFFLINE == 1
extern void EmbNET_CallBk_OFFLine(void); //chama rotina de de callback do usu�rio
#endif

#if CALLBK_FRAMESET == 1
extern void EmbNET_CallBk_FrameSet( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data );
#endif

#if CALLBK_FRAMEGET == 1
//extern void EmbNET_CallBk_FrameGet (uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t * Data );
extern void EmbNET_CallBk_FrameGet (uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t * DataReply, uint32_t DataValue, uint8_t *ValidReply);
#endif

#if CALLBK_FRAMEREPLY == 1
extern void EmbNET_CallBk_FrameReply( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data );
#endif

#if CALLBK_FRAMETRIGGER == 1
extern void EmbNET_CallBk_FrameTrigger( uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data );
#endif
#if CALLBK_FRAMESETDEVICE == 1
extern void EmbNET_CallBk_FrameSetDevice(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data);
#endif
#if CALLBK_FRAMEATTRIBERROR == 1
extern void EmbNET_CallBk_FrameAttribError(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID);
#endif
#if CALLBK_FRAMEREPLYERROR == 1
extern void EmbNET_CallBk_FrameReplyError(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID, uint32_t Data);
#endif
#if CALLBK_FRAMEGETERROR == 1
extern void EmbNET_CallBk_FrameGetError(uint8_t FDeviceID, uint8_t FNetID, uint16_t AttributeID);
#endif
#if SNIFFER == 1
extern void EmbNET_Sniffer(CAN_msg sMsg);
#endif



void EmbNET_StartUp(void);
void EmbNET_SetDevice(uint8_t sDeviceID, uint8_t sNetID, uint8_t Mode, uint16_t Class, uint16_t sSerial1, uint16_t sSerial2);
void EmbNET_SendLive(void);
void EmbNET_SendFrame_Set(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue);
void EmbNET_SendFrame_Get(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue);
void EmbNET_SendFrame_Trigger(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue);
void EmbNET_SendFrame_Reply(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue);
void EmbNET_SendFrame_AttribError(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID);
void EmbNET_SendFrame_SetDevice(uint8_t ToDeviceID, uint8_t ToNetID, uint16_t AttributeID, uint32_t AtributeValue);
void EmbNET_SendFrame_Discovery(void);
void EmbNET_TickTime100ms(void);





//=================================================================================================
//	ATRIBUTOS DA EMBNET
//=================================================================================================
/****** Atributos Fechados da EmbNet ******/
#define Attrib_Get_Version         	0x0100			//(r)Pega a vers�o da biblioteca EMBnet - OBRIGATORIO
#define Attrib_Set_Init_Slave      	0x0101			//(w)Envia os par�metros de inicializa��o do Slave - OBRIGATORIO
#define Attrib_Set_Turn_Off_Slave  	0x0102			//(w)Envia comando para colocar o Slave em modo offline - OBRIGATORIO
#define Attrib_Set_Reset_TimeOut   	0x0103			//(w)Envia par�metro de reset de timeout do Slave para que este n�o caia para offline - OBRIGATORIO
#define Attrib_Set_Time_Out_Slave  	0x0104			//(w/r)Envia par�metro de tempo para cair para offline - OBRIGATORIO
#define Attrib_EmbNet_Senha			0x0105			//(w)Envia a senha EmbNet que libera recursos de configura��o por tempo determinado. (digitos 0-5 senha, digitos 6-7 tempo maximo at� 60 seg.)
#define Attrib_DeviceID_NetID  		0x0120			//(w/r)Pega a o device ID e o Net ID do slave - OBRIGATORIO
#define Attrib_Serial				0x0121			//(w/r)N�mero serial do equipamento (16 bits para Lote + 16 bits para S�rie)
#define Attrib_Get_ClassId			0x0150			//(r)Leitura do ClassId

//Atributos de Erros da EmbNet
#define Attrib_EmbNet_Erro_Reset	0x0400			//(w)Reset dos contadores Parcial e Acumulado de erros da EmbNet
#define Attrib_EmbNet_Erro_Parcial	0x0401			//(r)Leitura dos erros ocorridos na EmbNet desde a energiza��o do m�dulo (0-7bits erros TX, 8-15bits erros de RX)
#define Attrib_EmbNet_Erro_Acumulado 0x0402			//(r)Leitura dos erros acumulados pelo m�dulo desde a ultima vez que foi zerado o hist�rico (0-15 erros TX, 16-31 erros RX)



/****** Atributos da EmbNet ******/
#define Attrib_Set_Reles           	0x1001			//Estabelece valores para saidas � rele
#define Attrib_Get_Reles           	0x1001			//Le os valores para saidas � rele
#define Attrib_Set_Out             	0x1002			//Estabelece valores para sa�das digitais
#define Attrib_Get_Out             	0x1002			//Le os valores para sa�das digitais
#define Attrib_Set_Counter		    0x1003			//Estabelece valores para contadores de tempo
#define Attrib_Get_Counter		    0x1003			//Le os valores para contadores de tempo
#define Attrib_Set_Leds            	0x1004			//Estabelece valores para para LEDs
#define Attrib_Get_Leds            	0x1004			//Le os valores para para LEDs
#define Attrib_Set_Alarmes			0x1005			//Estabelece valores e tipos de alarmes
#define Attrib_Get_Alarmes			0x1005			//Le os valores e tipos de alarmes
#define Attrib_Set_Flags			0x1006			//Estabelece valores para flags de opera��o interna bin�rias
#define Attrib_Get_Flags			0x1006			//Le os valores para flags de opera��o interna bin�rias
#define Attrib_Set_Modbus			0x1007			//Estabelece os comandos que ser�o enviados pelo modbus
#define Attrib_Set_DAC				0x1008			//Estabelece os valores para o DAC em passos do DAC
#define Attrib_Get_DAC				0x1008			//Le os os valores para o DAC em passos do DAC
#define Attrib_Set_Calibracao		0x1009			//Estabelece processo de calibra��o onde cada bit representa um dispositivo a ser calibrado
#define Attrib_Set_Configuracao		0x100A			//Estabelece par�metros de configura��o como modelo de equipamento, tipo de sensor a ser usado, sensores, saidas e entradas habilitadas, etc
#define Attrib_Get_Configuracao		0x100A			//Le os par�metros de configura��o como modelo de equipamento, tipo de sensor a ser usado, sensores, saidas e entradas habilitadas, etc
#define Attrib_Set_Parametros		0x100B			//Estabelece par�metros de configura��o tais como set point de temperatura, umidade,  histerese, etc
#define Attrib_Set_Vinculos			0x100C			//Define um vinculo
#define Attrib_Set_Triggers			0x100D			//Define um vinculo
#define Attrib_Set_PWM				0x100E			//Atualiza o valor de pwm de um canal especifico 2B-ch(0-99) 2B-%(0-100) 4B-Periodo(0-9999)*100ms


#define Attrib_Get_In              	0x5001			//Verifica o status das entradas binarias existentes
#define Attrib_Get_Status			0x5002			//Verifica o status de todas entradas e sa�das bin�rias do m�dulo (SP, NIVEL, RELES, SAIDAS DIGITAIS, etc)
#define Attrib_Get_Temperatura_1   	0x5003      //(r)Verifica o valor da temperatura 1
#define Attrib_Get_Temperatura_2   	0x5004			//(r)Verifica o valor da temperatura 2
#define Attrib_Get_Temperatura_3   	0x5005			//(r)Verifica o valor da temperatura 3
#define Attrib_Get_Temperatura_4   	0x5006			//(r)Verifica o valor da temperatura 4
#define Attrib_Get_Temperatura_5   	0x5007			//(r)Verifica o valor da temperatura 5
#define Attrib_Get_Temperatura_6   	0x5008			//(r)Verifica o valor da temperatura 6
#define Attrib_Get_Temperatura_7   	0x5009			//(r)Verifica o valor da temperatura 7
#define Attrib_Get_Temperatura_8   	0x500A			//(r)Verifica o valor da temperatura 8
#define Attrib_Get_Temperatura_9   	0x500B			//(r)Verifica o valor da temperatura 9
#define Attrib_Get_Temperatura_10  	0x500C			//(r)Verifica o valor da temperatura 10
#define Attrib_Get_Temperatura_11	0x500D			//(r)Verifica o valor da temperatura 11
#define Attrib_Get_Temperatura_12	0x500E			//(r)Verifica o valor da temperatura 12
#define Attrib_Get_Firmware			0x500F			//(r)Verifica a vers�o do firmware
#define Attrib_Get_VDC_1			0x5010			//(r)Verifica o valor de tens�o DC 1
#define Attrib_Get_VDC_2			0x5011			//(r)Verifica o valor de tens�o DC 2
#define Attrib_Get_VDC_3			0x5012			//(r)Verifica o valor de tens�o DC 3
#define Attrib_Get_VAC_1			0x5013			//(r)Verificar o valor de tens�o AC 1
#define Attrib_Get_VAC_2			0x5014			//(r)Verificar o valor de tens�o AC 2
#define Attrib_Get_VAC_3			0x5015			//(r)Verificar o valor de tens�o AC 3
#define Attrib_Get_Erros			0x5016			//(r)Verifica os erros existentes por modulo
#define Attrib_Get_Temp_Board		0x5017			//(r)Realiza a leitura da temperatura na placa
#define Attrib_Get_Contadores		0x5018			//(r)Realiza a leitura da temperatura dos contadores do m�dulo, este Get necessita de um parametro no DATA
#define Attrib_Teste				0x5019			//(w/r)Realiza a escrita e leitura do registrador de teste


#define Attrib_Analise_CAN			0x6000
#define Attrib_Analise_Registros	0x6001

#endif // _EMBNET_H_



/**********************************************************************
 * $Id$		Files.c			2017-05-02
 * @file	Files.c
 * @brief	Definicoes do hardware da placa multiponto
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 *
 ***********************************************************************
 * Driver desenvolvido para abstrair a biblioteca de FAT, caso resolva
 * alterar amanhã a biblioteca basta ajustar este driver.
 *
 **********************************************************************/
/*********************************************************************
 * 	ERRATA
 *
 *
 *
 **********************************************************************/

#include "ff.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "stdint.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

#include "Files.h"
#include "Files_tipos.h"

#include "board.h"




/**********************************************************************
 * @brief	Funcao de criptografia que bagunca os bits de cada char
 * @version	2.0
 * @date	02. Novembro. 2015
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pBuffer - buffer a ser criptografado
 *
 *
 * ERRATA
 * 		@date 	02-05-2017
 * 		@author	Ulysses C. Fonseca
 * 		@brief	Alterado o tipo de int para uint32_t para dar suporte a um buffer maior
 *
 *
 *		@date 	25-05-2017
 * 		@author	Ulysses C. Fonseca
 * 		@brief	Alterada a funcao para utilizar corretamente o ponteiro
 **********************************************************************/
//void emb_cripto(char *pBuffer){
//	char tmp;
//    while(*pBuffer){
//        tmp = *pBuffer;
//        if (tmp&0x02) {tmp&=~0x02;} else {tmp|=0x02;}
//        if (tmp&0x04) {tmp&=~0x04;} else {tmp|=0x04;}
//		if (tmp&0x20) {tmp&=~0x20;} else {tmp|=0x20;}
//        *pBuffer = tmp;
//        pBuffer++;
//    }
//}
void emb_cripto(char *pBuffer, uint32_t pSize){
	uint32_t i=0;
	for(i=0; i<pSize; i++){
		if (pBuffer[i]&0x02) {pBuffer[i]&=~0x02;} else {pBuffer[i]|=0x02;}
		if (pBuffer[i]&0x04) {pBuffer[i]&=~0x04;} else {pBuffer[i]|=0x04;}
		if (pBuffer[i]&0x20) {pBuffer[i]&=~0x20;} else {pBuffer[i]|=0x20;}
	}
}


void strip(char *s) {
    char *p2 = s;
    while(*s != '\0') {
        if(*s != '\t' && *s != '\n' && *s != '\r') {
            *p2++ = *s++;
        }
        else if(*s == '\r'){
            ++s;
        }
        else {
            *p2++ = ' ';
            ++s;
        }
    }
    *p2 = '\0';
}


/**********************************************************************
 * @brief	Cria um novo arquivo caso este não exista
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 * 		char *pPath - caminho do arquivo desejado
 *
 * @return
 * 		char
 * 			0 - sucess
 * 			1 - falha
 *
 **********************************************************************/
char createFile(char *pPath){
	FIL fsd;

    if(!f_open(&fsd, pPath, FA_CREATE_NEW)){
        f_sync(&fsd);
        f_close(&fsd);
    }
    else{return 1;}

    return 0;
}
/**********************************************************************
 * @brief	Cria uma nova pasta caso este não exista
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 * 		char *pPath - caminho da pasta desejada
 *
 * @return
 * 		char
 * 			0 - sucess
 * 			1 - falha
 *
 **********************************************************************/
char createFolder(char *pPath){

    if(f_mkdir(pPath)){return 1;}

    return 0;
}
/**********************************************************************
 * @brief	Deleta um arquivo
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 * 		char *pPath - caminho do arquivo desejado
 *
 * @return
 * 		char
 * 			0 - sucess
 * 			1 - falha
 *
 **********************************************************************/
char deleteFile(char *pPath){
    return f_unlink(pPath);
}
/**********************************************************************
 * @brief	Deleta um diretorio
 * @version	1.0
 * @date	22. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 * 		char *pPath - caminho do diretorio desejado
 *
 * @return
 * 		char
 * 			0 - sucess
 * 			1 - falha
 *
 **********************************************************************/
char deleteFolder(char *pPath){
    return f_unlink(pPath);
}
char deleteFolderRecursive(char *pPath){
	FRESULT fr;     /* Return value */
	DIR dj;         /* Directory search object */
	FILINFO fno;    /* File information */
	char path[50];


	fr = f_findfirst(&dj, &fno, pPath, "*");  /* Start to search for photo files */

	while (fr == FR_OK && fno.fname[0]) {         /* Repeat while an item is found */
		printf("del %s\n", fno.fname);                /* Display the object name */
		snprintf(path,sizeof(path),"%s/%s",pPath,fno.fname);
		if (fno.fattrib & AM_DIR) {    /* Item is a directory */
			fr = deleteFolderRecursive(path);
		} else {                        /* Item is a file */
			fr = f_unlink(path);
		}
		fr = f_findnext(&dj, &fno);               /* Search for next item */
	}

	if (fr == FR_OK) fr = f_unlink(pPath);  /* Delete the empty directory */
	f_closedir(&dj);
	return fr;
}

/**********************************************************************
 * @brief	Verifica se um arquivo existe
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 * 		char *pPath - caminho do arquivo desejado
 *
 * @return
 * 		char
 * 			0 - existente
 * 			1 - nao existe
 *
 **********************************************************************/
char checkFile(char *pPath){
	FIL fsd;

    if(!f_open(&fsd, pPath, FA_OPEN_EXISTING | FA_READ)){f_close(&fsd);}
    else{ return 1;}
    return 0;
}
/**********************************************************************
 * @brief	Escreve um dado em um arquivo, caso o arquivo nao exista
 * este sera criado
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 * 		char *pPath - caminho do arquivo desejado
 * 		char *pBuffer - dado a ser gravado
 * 		uint32_t pLength - tamanho do dado a ser gravado
 * 		uint16_t pPos -	posicao que devera ser gravado a partir do inicio do arquivo
 * 		uint8_t pCripto - utiliza criptografia
 *
 *
 * @return
 * 		char
 * 			0 - sucess
 * 			1 - falha
 *
 **********************************************************************/
char writeFile(char *pPath, char *pBuffer, uint32_t pLength, uint16_t pPos, uint8_t pCripto){
    FIL fsd;
    uint32_t cnt=0;

    //if(pCripto){emb_cripto(pBuffer,pLength);}
    //if(pCripto){emb_cripto(pBuffer);}

    if(checkFile(pPath)){createFile(pPath);}

    if(!f_open(&fsd, pPath, FA_OPEN_EXISTING | FA_WRITE)){
        f_lseek(&fsd, pPos);           //DESLOCAMENTO DO INICIO DO ARQUIVO PARA ARQUIVOS DE CABECALHO FUTURO
        f_write(&fsd, pBuffer, pLength, &cnt);
        f_sync(&fsd);
        f_close(&fsd);
    }
    else{return 1;}

    return 0;
}
/**********************************************************************
 * @brief	Realiza a leitura de um dado em um arquivo, caso o arquivo exista
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 * 		char *pPath - caminho do arquivo desejado
 * 		char *pBuffer - buffer que sera lido o arquivo
 * 		uint32_t pLength - tamanho do dado a ser lido
 * 		uint16_t pPos -	posicao que devera realizar a leitura a partir do inicio do arquivo
 * 		uint8_t pCripto - utiliza criptografia
 *
 *
 * @return
 * 		char
 * 			0 - sucess
 * 			1 - falha
 *
 **********************************************************************/
char readFile(char *pPath, char *pBuffer, uint32_t pLength, uint16_t pPos, uint8_t pCripto){
    FIL fsd;
    uint32_t cnt=0;

    if(!f_open(&fsd, pPath, FA_OPEN_EXISTING | FA_READ)){
    	f_lseek(&fsd, pPos);           //DESLOCAMENTO DO INICIO DO ARQUIVO PARA ARQUIVOS DE CABECALHO FUTURO
        f_read(&fsd, pBuffer, pLength, &cnt);
        f_sync(&fsd);
        f_close(&fsd);
    }
    else{return 1;}

    if(cnt != pLength){return 1;}

    //if(pCripto){emb_cripto(pBuffer,pLength);}
    //if(pCripto){emb_cripto(pBuffer);}

    return 0;
}
/**********************************************************************
 * @brief	Cria o arquivo check.dat para que seja feita a atualizacao da flash
 * @version	1.0
 * @date	20. Junho. 2017
 * @author	Ulysses C. Fonseca
 *
 * @return
 * 		char
 * 			0 - sucess
 * 			1 - falha
 *
 **********************************************************************/
char fcheck_boot(void){
	char text[10];

	snprintf(text, sizeof(text),"CHECK.DAT");
	emb_cripto((char*)&text,strlen(text));
	return writeFile("UPDATE/BIN/CHECK.DAT", (char*)&text, strlen(text), 0, 1);
}



char addRegistro(char *pPath, char *pBuffer, uint32_t pLength){
	FIL fsd;
	uint32_t cnt=0;

	if(!f_open(&fsd, pPath, FA_OPEN_EXISTING | FA_OPEN_APPEND | FA_WRITE)){
		f_write(&fsd, pBuffer, pLength, &cnt);
		f_sync(&fsd);
		f_close(&fsd);
	}
	else{return 1;}
	return 0;
}

char readRegistro(char *pPath, char *pBuffer, uint32_t pLength, uint32_t pPos){
	return readFile(pPath, pBuffer, pLength, pPos, 0);
}


/**********************************************************************
 * @brief	Funcao responsavel por realizar a compatibilidade com versoes diferentes de receitas
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       s_RecBravo *pRec - Receita a ser compatibilizada
 *       uint16_t pVersion - versao da estrutura lida
 *
 **********************************************************************/
void reestructRec(s_RecBravo *pRec, uint16_t pVersion){
    //TRATAR PARA CADA VERSAO DE ESTRUTURA

}
/**********************************************************************
 * @brief	Carrega a receita e trata incompatibilidades
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *       s_RecBravo *pRec - receita desejada onde seja salva a leitura
 *
 * @return
 * 		char
 *       0 - sucess
 *       1 - erro na leitura do cabecalho
 *       2 - erro na leitura do passo
 *       3 - tipo de estrutura incompativel
 *		 4 - versao da estrutura incompativel
 **********************************************************************/
char loadRec(char *pPath, s_RecBravo *pRec){
    uint8_t __p=0;
    uint8_t __reestruct=0;
    memset((char*)pRec,0x00, sizeof(pRec));

    if(readFile((char*)pPath, (char*)&pRec->cabecalho, sizeof(pRec->cabecalho), 64, 1)){ return 1;}
    if(pRec->cabecalho.type!=type_Bravo){return 3;}
    if(pRec->cabecalho.version<=version_Rec){__reestruct=1;}else{return 4;}

    //COMO A ESTRUTURA NAO E MAIOR QUE 32BITS PODE SER LIDO TUDO DE UMA VEZ
    if(readFile((char*)pPath, (char*)&pRec->passos, sizeof(pRec->passos), 64+512, 1)){ return 2;}

    for(__p=0; __p<MAX_PASSOS_REC; __p++){
        if(__reestruct){reestructRec(pRec,pRec->cabecalho.version);}
    }
    return 0;
}
/**********************************************************************
 * @brief	Salva a receita utilizando a estrutura atual
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *       s_RecBravo *pRec - receita desejada que seja salva
 *
 * @return
 *       0 - sucess
 *       1 - erro na leitura do cabecalho
 *       2 - erro na leitura do passo
 *       3 - tipo de estrutura incompativel
 *
 **********************************************************************/
char saveRec(char *pPath, s_RecBravo *pRec){
    uint8_t __p=0;
    uint8_t __passo_ativo=0;

    while(1){
        if(pRec->passos[__p].passo_ativo!=0){__passo_ativo++;}
        __p++;
        if(__p>MAX_PASSOS_REC){break;}
    }
    //COMO A ESTRUTURA NAO E MAIOR QUE 32BITS PODE SER GRAVADO TUDO DE UMA VEZ
    if(writeFile((char*)pPath, (char*)&pRec->passos, sizeof(pRec->passos), 64+512, 1)){ return 2;}

    pRec->cabecalho.total_passos = __passo_ativo;
    pRec->cabecalho.version = version_Rec;
    pRec->cabecalho.type = type_Bravo;

    if(writeFile((char*)pPath, (char*)&pRec->cabecalho, sizeof(pRec->cabecalho), 64, 1)){ return 1;}

    return 0;
}





/**********************************************************************
 * @brief	Adiciona um item na lista especificada
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *       s_Lista pList - item a ser adicionado na lista
 *
 * @return
 *       0 - sucess
 *       1 - falha
 *
 **********************************************************************/
char addItemList(char *pPath, s_Lista pList){
    uint32_t count[2]={0,0};
    uint8_t ret=0;
    if((ret = readFile((char*)pPath, (char*)&count, sizeof(count),0,1))){count[0]=0;}

    pList.version = version_Rec;
    pList.type = type_Bravo;

    if(writeFile((char*)pPath, (char*)&pList, sizeof(s_Lista), 64+(sizeof(s_Lista)*count[0]), 1)){ return 1;}
    count[0]+=1;
    count[1]+=1;
    if(writeFile((char*)pPath, (char*)&count, sizeof(count),0,1)){return 1;}

    return 0;

}
/**********************************************************************
 * @brief	Atualiza um item na lista especificada
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *       s_Lista pList - item a ser atualizado na lista
 *       uint8_t - index do item desejado
 *
 * @return
 *       0 - sucess
 *       1 - falha
 *
 **********************************************************************/
char uptItemList(char *pPath, s_Lista pList, uint8_t pIndex){

    if(writeFile((char*)pPath, (char*)&pList, sizeof(s_Lista), 64+(sizeof(s_Lista)*pIndex), 1)){ return 1;}

    return 0;

}
/**********************************************************************
 * @brief	Reposiciona um item na lista atual
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *       uint16_t pPos - item a ser reposicionado
 *       uint16_t pNewPos - nova posicao do item
 *
 * @return
 *       0 - sucess
 *       1 - erro no arquivo original
 *       2 - erro no arquivo temporario
 *       3 - index novo igual ao index atual
 *
 **********************************************************************/
char reposItemList(char *pPath, uint16_t pPos, uint16_t pNewPos){
    FIL fp;
    FIL fp_tmp;
    s_Lista record;
    s_Lista tmpRecord;
    char tmpPath[200];
    uint16_t sizePath = sizeof(pPath);
    uint16_t index=0;
    uint32_t count[2]={0,0};
    uint32_t cnt=0;
    uint8_t depois=0;

    memset((char*)tmpPath, 0x00, sizeof(tmpPath));
    memcpy((char*)tmpPath,pPath,sizePath);
    tmpPath[sizePath-3] = 'T';
    tmpPath[sizePath-2] = 'M';
    tmpPath[sizePath-1] = 'P';
    tmpPath[sizePath] = 0x00;


    if(pPos==pNewPos){return 3;}

    if(pPos<pNewPos){depois=1;}

    deleteFile(tmpPath);
    createFile(tmpPath);

    if (!f_open(&fp, pPath, FA_OPEN_EXISTING | FA_READ)) {
        if (!f_open(&fp_tmp, tmpPath, FA_OPEN_EXISTING | FA_WRITE)) {
        	f_lseek(&fp, 0);
        	f_lseek(&fp_tmp, 0);

            f_read(&fp, &count, sizeof(count), &cnt);
            f_write(&fp_tmp, &count, sizeof(count), &cnt);

            f_lseek(&fp, 64+sizeof(s_Lista)*pPos);
            f_read(&fp, &record,sizeof(s_Lista), &cnt);

            f_lseek(&fp, 64);
            f_lseek(&fp_tmp, 64);
            while (!f_read(&fp, &tmpRecord, sizeof(s_Lista), &cnt) && (cnt==sizeof(s_Lista))) {
                if(index==pNewPos){
                	if(depois){
                		f_write(&fp_tmp, &tmpRecord, sizeof(s_Lista), &cnt);
                		f_write(&fp_tmp, &record, sizeof(s_Lista), &cnt);
					}
					else{
						f_write(&fp_tmp, &record, sizeof(s_Lista), &cnt);
						f_write(&fp_tmp, &tmpRecord, sizeof(s_Lista), &cnt);
					}
                }
                else{
                    if(index!=pPos){
                    	f_write(&fp_tmp, &tmpRecord, sizeof(s_Lista), &cnt);
                    }
                }

                index++;
            }
            f_sync(&fp_tmp);
            f_close(&fp_tmp);
        }
        else{
        	return 2;
        }
        f_sync(&fp);
        f_close(&fp);
	}
    else{
    	return 1;
    }

    deleteFile(pPath);
	f_rename(tmpPath, pPath);


	return 0;
}
/**********************************************************************
 * @brief	Deleta um item na lista atual
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *       uint16_t pPos - item a ser deletado
 *
 * @return
 *       0 - sucess
 *       1 - erro no arquivo original
 *       2 - erro no arquivo temporario
 *       3 - pos inexistente
 *
 **********************************************************************/
char deleteItemList(char *pPath, uint16_t pPos){
	FIL fp;
	FIL fp_tmp;
    s_Lista tmpRecord;
    char tmpPath[200];
    uint16_t sizePath = sizeof(pPath);
    uint16_t index=0;
    uint32_t count[2]={0,0};
    uint32_t cnt=0;

    if(pPos>(readTotalId(pPath)-1)){return 3;}

    memset((char*)tmpPath, 0x00, sizeof(tmpPath));
    memcpy(&tmpPath,pPath,sizePath);
    tmpPath[sizePath-3] = 'T';
    tmpPath[sizePath-2] = 'M';
    tmpPath[sizePath-1] = 'P';
    tmpPath[sizePath] = 0x00;

    deleteFile(tmpPath);
    createFile(tmpPath);

    if (!f_open(&fp, pPath, FA_OPEN_EXISTING | FA_READ)) {
        if (!f_open(&fp_tmp, tmpPath, FA_CREATE_ALWAYS | FA_WRITE)) {
        	f_lseek(&fp, 0);
        	f_lseek(&fp_tmp, 0);

            //COPIA O COUNTER E ATUALIZA NO ARQUIVO TMP
            f_read(&fp, &count, sizeof(count), &cnt);
            if(count[0]>0){
            	count[0]-=1;
            }
            f_write(&fp_tmp, &count, sizeof(count), &cnt);

            //REPOSICIONA PARA FAZER A COPIA
            f_lseek(&fp, 64);
            f_lseek(&fp_tmp, 64);
            while (!f_read(&fp, &tmpRecord,sizeof(s_Lista), &cnt) && (cnt==sizeof(s_Lista))) {
                if(index!=pPos){
                    f_write(&fp_tmp, &tmpRecord, sizeof(s_Lista), &cnt);
                }
                index++;
            }
            f_sync(&fp_tmp);
            f_close(&fp_tmp);
        }
        else{
        	return 2;
        }
        f_sync(&fp);
        f_close(&fp);
	}
    else{
    	return 1;
    }

	deleteFile(pPath);          //deleta a lista original
	if(count[0]==0){
		deleteFile(tmpPath);          //deleta a lista temporaria
	}
	else{
		f_rename(tmpPath, pPath); 	//renomeia a lista temporaria para a original
	}

	return 0;
}
/**********************************************************************
 * @brief	Realiza a leitura do proximo ID na lista especificada
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *
 * @return
 *       uint32_t - proximo ID a ser utilizado
 *
 **********************************************************************/
uint32_t readNextId(char *pPath){
    uint32_t count[2]={0,0};

    if(readFile((char*)pPath, (char*)&count, sizeof(count),0,1)){ count[1] = 0;}

    return count[1];
}
/**********************************************************************
 * @brief	Realiza a leitura do total de ID na lista especificada
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *
 * @return
 *       uint32_t - total de IDs
 *
 **********************************************************************/
uint32_t readTotalId(char *pPath){
    uint32_t count[2] = {0,0};

    if(readFile((char*)pPath, (char*)&count, sizeof(count),0,1)){ count[0] = 0;}

    return count[0];
}
/**********************************************************************
 * @brief	Realiza a leitura de um item na lista especificada
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 * @param
 *       char *pPath - endereco do arquivo desejado
 *       s_Lista *pList - item lido da lista
 *       uint16_t pPos - posicao a ler o item
 *
 * @return
 *       0 - sucess
 *       1 - falha
 *
 * ERRATA
 * 		@date 	03-05-2017
 * 		@author Ulysses C. Fonseca
 * 		@brief	implementada logica para nao aceitar pPos maior que o total do arquivo
 *
 **********************************************************************/
char readItemList(char *pPath, s_Lista *pList, uint16_t pPos){
	if(pPos>(readTotalId(pPath)-1)){return 1;}
	if(readFile((char*)pPath, (char*)pList, sizeof(s_Lista), 64+(sizeof(s_Lista)*pPos),1)){return 1;}
	return 0;
}

char readItemList_other(char *pPath, void *pList, uint32_t pSize, uint16_t pPos){
	if(pPos>(readTotalId(pPath)-1)){return 1;}
	if(readFile((char*)pPath, (char*)pList, pSize, 64+(pSize*pPos),1)){return 1;}
	return 0;
}



/**********************************************************************
 * @brief 	Verifica se os dois arquivos possuem o mesmo tamanho
 * @version 1.1
 * @date 	29. Maio. 2017
 * @author 	Ulysses C. Fonseca
 *
 * @param
 * 		char *file_1 - arquivo de origem
 * 		char *file_2 - arquivo de destino
 *
 *	@return
 *		0 - tamanho igual
 *		1 - tamanho diferente
 *		2 - arquivo 1 nao encontrado
 *		3 - arquivo 2 nao encontrado
 *
 *********************************************************************/
char fverify_size(char *file_1,char *file_2){
	FIL f_1;
	FIL f_2;
	long int size_1=0, size_2=0;

	if (!f_open(&f_1, file_1, FA_OPEN_EXISTING | FA_READ)) {
		size_1 = f_size(&f_1);
		f_sync(&f_1);
		f_close(&f_1);
	}
	else{return 2;}

	if (!f_open(&f_2, file_2, FA_OPEN_EXISTING | FA_READ)) {
		size_2 = f_size(&f_2);
		f_sync(&f_2);
		f_close(&f_2);
	}
	else{return 3;}


	if(size_2>=size_1){ return 0;}
	else{return 1;}
}
char fverify_files(char *file_1,char *file_2){
	FIL f_1;
	FIL f_2;
	char buf1[128];
	char buf2[128];
	UINT c=0, d=0;
	char ret=0;

	if (!f_open(&f_1, file_1, FA_OPEN_EXISTING | FA_READ)) {
		if (!f_open(&f_2, file_2, FA_OPEN_EXISTING | FA_READ)) {
			for(;;){
				f_read(&f_1, &buf1, sizeof(buf1), &c);
				if(c==0){break;}
				f_read(&f_2, &buf2, sizeof(buf2), &d);

				if(c!=d){ret=1; break;}
				if(memcmp(&buf1, &buf2, sizeof(buf2))){ret=1; break;}//VERIFICA SE O ARQUIVO E IGUAL
			}
			f_sync(&f_2);
			f_close(&f_2);
		}
		else{return 3;}
		f_sync(&f_1);
		f_close(&f_1);
	}
	else{return 2;}


	return ret;
}

char fget_size_with_struct_list(char *file_1, uint16_t pSize){
	FIL f_1;
	long int size_1=0;

	if (!f_open(&f_1, file_1, FA_OPEN_EXISTING | FA_READ)) {
		size_1 = f_size(&f_1);
		f_sync(&f_1);
		f_close(&f_1);
	}
	else{return 2;}

	size_1 -= 64; //REMOVE HASH
	return (size_1%pSize);

}


/**********************************************************************
 * @brief 	Copia um arquivo para outro destino
 * @version 2.0
 * @date 	29. Maio. 2017
 * @author 	Ulysses C. Fonseca
 *
 * @param
 * 		f_origem - endereço e nome do arquivo de origem
 * 		f_destino - endereço e nome do arquivo de destino
 *
 * @return
 * 		0 - sucesso
 * 		1 - erro na leitura do arquivo de origem
 * 		2 - erro na escrita do arquivo de destino
 * 		3 - arquivo gerado diferente do arquivo de origem(é deletado o destino)
 *
 **********************************************************************/
char copy_file(char *f_origem, char *f_destino){

	FIL fo;
	FIL fd;
	char pCopy[128];   	/* File copy buffer */
	char pCopyd[128];   /* File copy buffer */
	char ret=0;
	uint32_t cnt=0;
	uint32_t dnt=0;

//	console_sendString("COPY FILE\n\r");

	if(!f_open(&fo, f_origem, FA_OPEN_EXISTING | FA_READ)){
		if(!f_open(&fd, f_destino, FA_CREATE_ALWAYS | FA_WRITE)){
			f_read(&fo, &pCopy, sizeof(pCopy), &cnt);
			while(cnt){
				f_write(&fd, &pCopy, cnt, &dnt);
				f_read(&fo, &pCopy, sizeof(pCopy), &cnt);
				vTaskDelay(1);
//				console_sendString("*");
			}

			f_sync(&fd);
			f_close(&fd);
		}
		else{ret=3;}

		f_sync(&fo);
		f_close(&fo);
	}
	else{ret=2;}


	if(ret){deleteFile(f_destino);}
	if(ret){return ret;}

//	console_sendString("VERIFICANDO \n\r");
	//VERIFY FILE COPY IS EQUAL OF ORIGINAL
	if(!f_open(&fo, f_origem, FA_OPEN_EXISTING | FA_READ)){
		if(!f_open(&fd, f_destino, FA_OPEN_EXISTING | FA_READ)){

			f_read(&fo, &pCopy, sizeof(pCopy), &cnt);
			while(cnt){
				f_read(&fd, &pCopyd, sizeof(pCopyd), &dnt);
//				console_sendString("*");
				if(memcmp(&pCopy, &pCopyd, sizeof(pCopy))){ret=1; cnt=0;}//VERIFICA SE O ARQUIVO E IGUAL
				else{f_read(&fo, &pCopy, sizeof(pCopy), &cnt);}
				vTaskDelay(1);
			}

			f_sync(&fd);
			f_close(&fd);
		}
		else{ret=3;}
		f_sync(&fo);
		f_close(&fo);
	}
	else{ret=2;}


	if(ret){deleteFile(f_destino);}

	return ret;
}

char file_copy(char * fOrigem, char * fDestino){
	FIL fo;
	FIL fd;
	char buff[128];
	uint32_t cnt=0;
	uint32_t dnt=0;

	if(!f_open(&fo, fOrigem, FA_OPEN_EXISTING | FA_READ)){
		if(!f_open(&fd, fDestino, FA_CREATE_ALWAYS | FA_WRITE)){

			f_read(&fo, &buff, sizeof(buff), &cnt);
			while(cnt){
				f_write(&fd, &buff, cnt, &dnt);
				f_read(&fo, &buff, sizeof(buff), &cnt);
			}
//			console_sendString("copy sucessfull\n\r");
			f_sync(&fd);
			f_close(&fd);
		}
		f_sync(&fo);
		f_close(&fo);
	}

	return 0;
}



const char PROJETO[]="BRAVO";
const char VERSAO[] = "1.0.0";
volatile uint64_t __freeSpace_SD=0;
void set_free_space_sd(uint64_t pFreeSpace){
	__freeSpace_SD = pFreeSpace;
}
/**********************************************************************
 * @brief 	Verificação do arquivo de apoio de atualização
 * @version 1.1
 * @date 	29. Maio. 2017
 * @author 	Marcos Vinicius
 *
 * @param
 * 		uint16_t *Num_Arquivos - numero de arquivos para atualizacao
 * 		uint32_t *Tamanho_Bytes - total de bytes a serem atualizados
 *
 *	@return
 *		0 - tamanho igual
 *		1 - tamanho diferente
 *		2 - arquivo 1 nao encontrado
 *		3 - arquivo 2 nao encontrado
 *
 * ERRATA
 * 		@date 	 29-05-2017
 * 		@version 1.0
 * 		@author  Ulysses C. Fonseca
 * 		@brief 	 modificada para compatibilizar com a FatFS
 *
 *********************************************************************/
int fupdate_check_info(uint16_t *Num_Arquivos, uint32_t *Tamanho_Bytes){

	#define INFO_PROJETO 			1
	#define INFO_NUM_ARQUIVOS 		2
	#define INFO_TAMANHO_BYTES		3
	#define INFO_VERSAO 			4
	#define INFO_MD5SUM				5

	FIL fsd;
	uint8_t info_code=0; //static
	char buf[100];
	char *param;
	char *p;
	uint16_t index=0;
	UINT c=0;
	uint64_t tamanho_minimo_sd=0;

	memset(buf,0,100);

	if (!f_open(&fsd, "1:UPDATE/INFO.DAT", FA_OPEN_EXISTING | FA_READ)) {
		f_read(&fsd, buf, sizeof(buf), &c);
		f_sync(&fsd);
		f_close(&fsd);
	}




    emb_cripto(buf,c);
    //param = (char*)malloc(c);
    //p = (char*)malloc(c);
    p = buf;

    while(*p){
        while (*p==';')p++;
        param = p;
        while (*p!=';' && *p!='\n'){p++; c--;}
        *p++='\0';
        c--;

        //NOVO REGISTRO
        index = strlen(param);
        info_code++;
        switch (info_code)
        {
            case INFO_PROJETO:
                if (memcmp(PROJETO,param,index)!=0){/*free(p); free(param);*/ return 2;} 		//Erro 2 - Projetos Incompatíveis
            break;
            case INFO_NUM_ARQUIVOS:
                *Num_Arquivos=atoi(param);
            break;
            case INFO_TAMANHO_BYTES:
                *Tamanho_Bytes=atoi(param);
                tamanho_minimo_sd = __freeSpace_SD;
                tamanho_minimo_sd+=100000; // Tamanho mínimo necessário = espaço livre no cartão + 100KB
//                snprintf(text,sizeof(text)," %d %d\n\r",tamanho_minimo_sd, *Tamanho_Bytes);
//                console_sendString(text);
                if (tamanho_minimo_sd<(*Tamanho_Bytes)) {/*free(p); free(param);*/ return 3;} 	//Erro 3 - Espaço insuficiente no cartão SD
            break;
            case INFO_VERSAO:
            	if (memcmp(VERSAO,param,index)==0){/*free(p); free(param);*/return 4;}			//Erro 4 - Firmware atual com mesma versão do novo firmware
            break;
        }
        //FIM NOVO REGISTRO

        if(!c){ break;}
    }

//    console_sendString("\n\rSAIU DA FUNCAO\n\r");
    //free(p);
    //free(param);

    return 0;
}



char* substr(char *buff, uint8_t start,uint8_t len, char* substr){
    //strncpy(substr, buff+start, len);
    memcpy(substr, buff+start, len);
    substr[len] = 0;
    return substr;
}
uint16_t find_char(char *str, char *substr, uint16_t start){
    uint16_t i=0;
    while(*str){
        if(*str==*substr && i>=start){return i;}
        i++;
        str++;
    }

    return 0;
}
void _folder_recursive(char *path){
	uint8_t pos=0;
	uint8_t last_pos=0;
	int8_t delta=0;
	char text[50];

	for(;;){
		pos = find_char(path,"/",last_pos);
		delta = pos - last_pos;
		if(delta<0){break;}
		else{
			substr(path,last_pos,delta, (char*)&text);

//			console_sendString("\n\rCREATE FOLDER\t");
//			console_sendString(text);


			//CRIA O DIRETORIO E ENTRA NELE
			f_mkdir((char*)text);
			f_chdir((char*)text);

//			console_sendString("\t");
//			f_getcwd((char*)text, sizeof(text));
//			console_sendString(text);
//			console_sendString("\n\r");

			last_pos = pos+1;
		}
	}
	f_chdir("/");
	f_getcwd((char*)text, sizeof(text));
//	console_sendString((uint8_t*)text);
//	console_sendString((uint8_t*)"\n\r");
}
void strip_barra(char *s) {
    while(*s != '\0') {
        if(*s == '\\'){
            *s = '/';
        }
        s++;
    }
}
char _check_files_USB(char *file){
    uint8_t tick_erro = 0;
    uint8_t tick_tentativa = 10;
    char file_dest[128];
    snprintf(file_dest,sizeof(file_dest),"1:%s",file);
    do{
        //printf("%s %s\n",file,file_dest);
        tick_erro = checkFile(file_dest);
        tick_tentativa--;
    }while(tick_erro && tick_tentativa);

    if(tick_erro){return 6;}
    return 0;
}
char _copy_files_Update(char *file){
	char file_orig[128];
    uint8_t tick_erro = 0;
    uint8_t tick_tentativa = 10;

    snprintf(file_orig,sizeof(file_orig),"%s",file);
    _folder_recursive(file_orig);
    snprintf(file_orig,sizeof(file_orig),"1:%s",file);
    do{
    	tick_erro = copy_file(file_orig,file);
        tick_tentativa--;
    }while(tick_erro && tick_tentativa);

    if(tick_erro){return tick_erro+8;}
    return 0;
}
int UpdateCheck_Files_Upgrade(uint8_t *pUpdateScr, uint8_t pUpdate, uint16_t *pTotalFiles){
	FIL     fsd;
	char    buf[256];
	uint16_t i=0;
    uint8_t  loop=1;
    int      ret=0;
	uint16_t total_files=0;
	uint16_t c=0;
//	uint16_t r=0;
	uint64_t next_pos=0;



	while(loop){
        ret = 0;
        memset(buf,0,sizeof(buf));
        if (!f_open(&fsd, "1:UPDATE/LIST.DAT", FA_OPEN_EXISTING | FA_READ)) {
        	f_lseek(&fsd, next_pos);
        	f_read(&fsd, buf, sizeof(buf), (UINT*)&c);
//        	r = c;
        	f_sync(&fsd);
        	f_close(&fsd);
        }
        else{
        	loop = 0;
        	return 5;
        }

        if(!c){loop=0; break;}

        emb_cripto(buf,c);
        //p = malloc(c);

        i=0;
        while(buf[i]!=';'){i++;}
        buf[i] = '\0';
        strip_barra(buf);
        next_pos += i+1;
//        console_sendString(buf);
//        console_sendString("\t");

        if(pUpdate){    //COPIA OS ARQUIVOS PARA O CARTAO
        	ret = _copy_files_Update(buf);
        }
        else{           //VERIFICA OS ARQUIVOS DO USB
        	ret = _check_files_USB(buf);
        }
        total_files++;
        *pTotalFiles = total_files;

        if(ret){/*free(p);*/return ret;}
        *pUpdateScr=1;
        while(*pUpdateScr){vTaskDelay(1);}
	}

//	console_sendString("\n\r\n\rFINALIZADO\n\r\n\r");

	return 0;
}


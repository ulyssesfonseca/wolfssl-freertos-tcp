/*-----------------------------------------------------------------------*/
/* Control functions for SD Card								         */
/*-----------------------------------------------------------------------*/

#include "sdControl.h"

extern uint32_t timerCntms;

SDMMC_CARD_T sdCardInfo;
SDMMC_EVENT_T *event;
volatile Status  eventResult = ERROR;
volatile int32_t sdcWaitExit = 0;

/* Delay callback for timed SDIF/SDMMC functions */
void waitMs(uint32_t time){
	uint32_t init = timerCntms;

	/* In an RTOS, the thread would sleep allowing other threads to run.
	   For standalone operation, we just spin on a timer */
	while (timerCntms < init + time) {}
//	while(time){time--;}
}

/**
 * @brief	Sets up the event driven wakeup
 * @param	pEvent : Event information
 * @return	Nothing
 */
void setupEvWakeup(void *pEvent){
#ifdef SDC_DMA_ENABLE
	/* Wait for IRQ - for an RTOS, you would pend on an event here with a IRQ based wakeup. */
	NVIC_ClearPendingIRQ(DMA_IRQn);
#endif
	event = (SDMMC_EVENT_T *)pEvent;
	sdcWaitExit = 0;
	eventResult = ERROR;
#ifdef SDC_DMA_ENABLE
	NVIC_EnableIRQ(DMA_IRQn);
#endif /*SDC_DMA_ENABLE*/
}

/**
 * @brief	A better wait callback for SDMMC driven by the IRQ flag
 * @return	0 on success, or failure condition (Nonzero)
 */
uint32_t waitEvIRQDriven(void){
	/* Wait for event, would be nice to have a timeout, but keep it  simple */
	while (sdcWaitExit == 0) {}
	if (eventResult) {
		return 0;
	}

	return 1;
}

#ifdef SDC_DMA_ENABLE
/******************************************************************************
 **  DMA Handler
 ******************************************************************************/
void DMA_IRQHandler (void){
	eventResult = Chip_GPDMA_Interrupt(LPC_GPDMA, event->DmaChannel);
	sdcWaitExit = 1;
	NVIC_DisableIRQ(DMA_IRQn);
}
#endif

/**
 * @brief	SDC interrupt handler sub-routine
 * @return	Nothing
 */
void SDIO_IRQHandler(void){
	int32_t Ret;
#ifdef SDC_DMA_ENABLE
	Ret = Chip_SDMMC_IRQHandler(LPC_SDC, NULL,0,NULL,0);
#else
	if(event->Index < event->Size) {
		if(event->Dir) { /* receive */
			Ret = Chip_SDMMC_IRQHandler(LPC_SDC, NULL,0,(uint8_t*)event->Buffer,&event->Index);
		}
		else {
			Ret = Chip_SDMMC_IRQHandler(LPC_SDC, (uint8_t*)event->Buffer,&event->Index,NULL,0);
		}
	}
	else {
		Ret = Chip_SDMMC_IRQHandler(LPC_SDC, NULL,0,NULL,0);
	}
#endif
	if(Ret < 0) {
		eventResult = ERROR;
		sdcWaitExit = 1;
	}
#ifndef SDC_DMA_ENABLE
	else if(Ret == 0) {
		eventResult = SUCCESS;
		sdcWaitExit = 1;
	}
#endif
}

/* Initialize SD/MMC */
void Init_SDMMC(void){
	memset(&sdCardInfo, 0, sizeof(sdCardInfo));
	sdCardInfo.evsetup_cb = setupEvWakeup;
	sdCardInfo.waitfunc_cb = waitEvIRQDriven;
	sdCardInfo.msdelay_func = waitMs;

//	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 19, (IOCON_FUNC2 | IOCON_MODE_INACT)); 	//MCICLK
//	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 20, (IOCON_FUNC2 | IOCON_MODE_INACT)); 	//MCICMD
//	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 21, (IOCON_FUNC2 | IOCON_MODE_INACT)); 	//MCIPWR
//	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 22, (IOCON_FUNC2 | IOCON_MODE_INACT)); 	//MCIDAT0
//	Chip_IOCON_PinMuxSet(LPC_IOCON, 2, 11, (IOCON_FUNC2 | IOCON_MODE_INACT));	//MCIDAT1
//	Chip_IOCON_PinMuxSet(LPC_IOCON, 2, 12, (IOCON_FUNC2 | IOCON_MODE_INACT));	//MCIDAT2
//	Chip_IOCON_PinMuxSet(LPC_IOCON, 2, 13, (IOCON_FUNC2 | IOCON_MODE_INACT));	//MCIDAT3

	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 2, (IOCON_FUNC2 | IOCON_MODE_INACT)); 	//MCICLK
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 3, (IOCON_FUNC2 | IOCON_MODE_INACT)); 	//MCICMD
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 5, (IOCON_FUNC2 | IOCON_MODE_INACT)); 	//MCIPWR
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 6, (IOCON_FUNC2 | IOCON_MODE_INACT)); 	//MCIDAT0
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 7, (IOCON_FUNC2 | IOCON_MODE_INACT));	//MCIDAT1
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 11, (IOCON_FUNC2 | IOCON_MODE_INACT));	//MCIDAT2
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 12, (IOCON_FUNC2 | IOCON_MODE_INACT));	//MCIDAT3

	LPC_SYSCTL->SCS &= ~(1 << 3);

	Chip_SDC_Init(LPC_SDC);

	/* Enable SD interrupt */
	NVIC_EnableIRQ(SDC_IRQn);
}

void DeInit_SDMMC(void){

	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 2, (IOCON_FUNC0 | IOCON_MODE_INACT)); 	//MCICLK
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 3, (IOCON_FUNC0 | IOCON_MODE_INACT)); 	//MCICMD
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 5, (IOCON_FUNC0 | IOCON_MODE_INACT)); 	//MCIPWR
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 6, (IOCON_FUNC0 | IOCON_MODE_INACT)); 	//MCIDAT0
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 7, (IOCON_FUNC0 | IOCON_MODE_INACT));	//MCIDAT1
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 11, (IOCON_FUNC0 | IOCON_MODE_INACT));	//MCIDAT2
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 12, (IOCON_FUNC0 | IOCON_MODE_INACT));	//MCIDAT3

	LPC_SYSCTL->SCS |= (1 << 3);

	Chip_SDC_DeInit(LPC_SDC);

	/* Disable SD interrupt */
	NVIC_DisableIRQ(SDC_IRQn);
}

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
EXECUTABLES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
FreeRTOS-Plus-TCP \
FreeRTOS-Plus-TCP/portable/BufferManagement \
FreeRTOS-Plus-TCP/portable/NetworkInterface/LPC17xx \
JSON/src \
WolfSSL/ctaocrypt/src \
WolfSSL/src \
WolfSSL/wolfcrypt/benchmark \
WolfSSL/wolfcrypt/src \
WolfSSL/wolfcrypt/src/port/pic32 \
WolfSSL/wolfcrypt/src/port/ti \
WolfSSL/wolfcrypt/test \
app/src \
board/src \
can/src \
embnet/src \
emwin/Config \
emwin/System/HW \
emwin/System \
fatfs/src \
freertos \
freertos/portable/gcc/ARM_CM3 \
freertos/portable \
lpcusblib/Drivers/USB/Class/Common \
lpcusblib/Drivers/USB/Class/Host \
lpcusblib/Drivers/USB/Core \
lpcusblib/Drivers/USB/Core/HAL/LPC17XX \
lpcusblib/Drivers/USB/Core/HCD \
lpcusblib/Drivers/USB/Core/HCD/OHCI \
mcp9803/src \
mcu/src \
panel_touch_FT5216/src \
utilities/src \


################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Config/bk/GUIConf.c \
../Config/bk/GUI_X.c \
../Config/bk/LCDConf.c \
../Config/bk/SIMConf.c 

OBJS += \
./Config/bk/GUIConf.o \
./Config/bk/GUI_X.o \
./Config/bk/LCDConf.o \
./Config/bk/SIMConf.o 

C_DEPS += \
./Config/bk/GUIConf.d \
./Config/bk/GUI_X.d \
./Config/bk/LCDConf.d \
./Config/bk/SIMConf.d 


# Each subdirectory must supply rules for building sources it contributes
Config/bk/%.o: ../Config/bk/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -D__USE_CMSIS -DDEBUG -D__CODE_RED -I"C:\NXP\emWin22\NXP_emWinBSP_EA1788_LPCXpresso422_Keil421_Crossworks21_MSVS2010_110412\Start\Application" -I"C:\NXP\emWin22\NXP_emWinBSP_EA1788_LPCXpresso422_Keil421_Crossworks21_MSVS2010_110412\Start\Config" -I"C:\NXP\emWin22\NXP_emWinBSP_EA1788_LPCXpresso422_Keil421_Crossworks21_MSVS2010_110412\Start\GUI" -I"C:\NXP\emWin22\NXP_emWinBSP_EA1788_LPCXpresso422_Keil421_Crossworks21_MSVS2010_110412\Start\Inc" -I"C:\NXP\emWin22\NXP_emWinBSP_EA1788_LPCXpresso422_Keil421_Crossworks21_MSVS2010_110412\Start\System\HW" -I"C:\NXP\emWin22\NXP_emWinBSP_EA1788_LPCXpresso422_Keil421_Crossworks21_MSVS2010_110412\Start\System\HW\DeviceSupport" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m4 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



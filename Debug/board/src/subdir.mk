################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../board/src/board.c \
../board/src/board_pinout.c \
../board/src/board_sysinit.c \
../board/src/cr_redlib_heap_fix.c \
../board/src/cr_startup_lpc177x_8x.c \
../board/src/sysinit.c 

OBJS += \
./board/src/board.o \
./board/src/board_pinout.o \
./board/src/board_sysinit.o \
./board/src/cr_redlib_heap_fix.o \
./board/src/cr_startup_lpc177x_8x.o \
./board/src/sysinit.o 

C_DEPS += \
./board/src/board.d \
./board/src/board_pinout.d \
./board/src/board_sysinit.d \
./board/src/cr_redlib_heap_fix.d \
./board/src/cr_startup_lpc177x_8x.d \
./board/src/sysinit.d 


# Each subdirectory must supply rules for building sources it contributes
board/src/%.o: ../board/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -DWOLFSSL_USER_SETTINGS -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\JSON\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\freertos\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\FreeRTOS-Plus-TCP\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\FreeRTOS-Plus-TCP\portable\Compiler\GCC" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\freertos\portable\gcc\ARM_CM3" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\WolfSSL" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



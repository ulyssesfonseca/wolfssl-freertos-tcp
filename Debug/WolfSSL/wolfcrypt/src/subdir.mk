################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../WolfSSL/wolfcrypt/src/aes.c \
../WolfSSL/wolfcrypt/src/arc4.c \
../WolfSSL/wolfcrypt/src/asm.c \
../WolfSSL/wolfcrypt/src/asn.c \
../WolfSSL/wolfcrypt/src/blake2b.c \
../WolfSSL/wolfcrypt/src/camellia.c \
../WolfSSL/wolfcrypt/src/chacha.c \
../WolfSSL/wolfcrypt/src/chacha20_poly1305.c \
../WolfSSL/wolfcrypt/src/coding.c \
../WolfSSL/wolfcrypt/src/compress.c \
../WolfSSL/wolfcrypt/src/curve25519.c \
../WolfSSL/wolfcrypt/src/des3.c \
../WolfSSL/wolfcrypt/src/dh.c \
../WolfSSL/wolfcrypt/src/dsa.c \
../WolfSSL/wolfcrypt/src/ecc.c \
../WolfSSL/wolfcrypt/src/ecc_fp.c \
../WolfSSL/wolfcrypt/src/ed25519.c \
../WolfSSL/wolfcrypt/src/error.c \
../WolfSSL/wolfcrypt/src/fe_low_mem.c \
../WolfSSL/wolfcrypt/src/ge_low_mem.c \
../WolfSSL/wolfcrypt/src/ge_operations.c \
../WolfSSL/wolfcrypt/src/hash.c \
../WolfSSL/wolfcrypt/src/hc128.c \
../WolfSSL/wolfcrypt/src/hmac.c \
../WolfSSL/wolfcrypt/src/integer.c \
../WolfSSL/wolfcrypt/src/logging.c \
../WolfSSL/wolfcrypt/src/md2.c \
../WolfSSL/wolfcrypt/src/md4.c \
../WolfSSL/wolfcrypt/src/md5.c \
../WolfSSL/wolfcrypt/src/memory.c \
../WolfSSL/wolfcrypt/src/misc.c \
../WolfSSL/wolfcrypt/src/pkcs7.c \
../WolfSSL/wolfcrypt/src/poly1305.c \
../WolfSSL/wolfcrypt/src/pwdbased.c \
../WolfSSL/wolfcrypt/src/rabbit.c \
../WolfSSL/wolfcrypt/src/random.c \
../WolfSSL/wolfcrypt/src/ripemd.c \
../WolfSSL/wolfcrypt/src/rsa.c \
../WolfSSL/wolfcrypt/src/sha.c \
../WolfSSL/wolfcrypt/src/sha256.c \
../WolfSSL/wolfcrypt/src/sha512.c \
../WolfSSL/wolfcrypt/src/tfm.c \
../WolfSSL/wolfcrypt/src/wc_port.c 

OBJS += \
./WolfSSL/wolfcrypt/src/aes.o \
./WolfSSL/wolfcrypt/src/arc4.o \
./WolfSSL/wolfcrypt/src/asm.o \
./WolfSSL/wolfcrypt/src/asn.o \
./WolfSSL/wolfcrypt/src/blake2b.o \
./WolfSSL/wolfcrypt/src/camellia.o \
./WolfSSL/wolfcrypt/src/chacha.o \
./WolfSSL/wolfcrypt/src/chacha20_poly1305.o \
./WolfSSL/wolfcrypt/src/coding.o \
./WolfSSL/wolfcrypt/src/compress.o \
./WolfSSL/wolfcrypt/src/curve25519.o \
./WolfSSL/wolfcrypt/src/des3.o \
./WolfSSL/wolfcrypt/src/dh.o \
./WolfSSL/wolfcrypt/src/dsa.o \
./WolfSSL/wolfcrypt/src/ecc.o \
./WolfSSL/wolfcrypt/src/ecc_fp.o \
./WolfSSL/wolfcrypt/src/ed25519.o \
./WolfSSL/wolfcrypt/src/error.o \
./WolfSSL/wolfcrypt/src/fe_low_mem.o \
./WolfSSL/wolfcrypt/src/ge_low_mem.o \
./WolfSSL/wolfcrypt/src/ge_operations.o \
./WolfSSL/wolfcrypt/src/hash.o \
./WolfSSL/wolfcrypt/src/hc128.o \
./WolfSSL/wolfcrypt/src/hmac.o \
./WolfSSL/wolfcrypt/src/integer.o \
./WolfSSL/wolfcrypt/src/logging.o \
./WolfSSL/wolfcrypt/src/md2.o \
./WolfSSL/wolfcrypt/src/md4.o \
./WolfSSL/wolfcrypt/src/md5.o \
./WolfSSL/wolfcrypt/src/memory.o \
./WolfSSL/wolfcrypt/src/misc.o \
./WolfSSL/wolfcrypt/src/pkcs7.o \
./WolfSSL/wolfcrypt/src/poly1305.o \
./WolfSSL/wolfcrypt/src/pwdbased.o \
./WolfSSL/wolfcrypt/src/rabbit.o \
./WolfSSL/wolfcrypt/src/random.o \
./WolfSSL/wolfcrypt/src/ripemd.o \
./WolfSSL/wolfcrypt/src/rsa.o \
./WolfSSL/wolfcrypt/src/sha.o \
./WolfSSL/wolfcrypt/src/sha256.o \
./WolfSSL/wolfcrypt/src/sha512.o \
./WolfSSL/wolfcrypt/src/tfm.o \
./WolfSSL/wolfcrypt/src/wc_port.o 

C_DEPS += \
./WolfSSL/wolfcrypt/src/aes.d \
./WolfSSL/wolfcrypt/src/arc4.d \
./WolfSSL/wolfcrypt/src/asm.d \
./WolfSSL/wolfcrypt/src/asn.d \
./WolfSSL/wolfcrypt/src/blake2b.d \
./WolfSSL/wolfcrypt/src/camellia.d \
./WolfSSL/wolfcrypt/src/chacha.d \
./WolfSSL/wolfcrypt/src/chacha20_poly1305.d \
./WolfSSL/wolfcrypt/src/coding.d \
./WolfSSL/wolfcrypt/src/compress.d \
./WolfSSL/wolfcrypt/src/curve25519.d \
./WolfSSL/wolfcrypt/src/des3.d \
./WolfSSL/wolfcrypt/src/dh.d \
./WolfSSL/wolfcrypt/src/dsa.d \
./WolfSSL/wolfcrypt/src/ecc.d \
./WolfSSL/wolfcrypt/src/ecc_fp.d \
./WolfSSL/wolfcrypt/src/ed25519.d \
./WolfSSL/wolfcrypt/src/error.d \
./WolfSSL/wolfcrypt/src/fe_low_mem.d \
./WolfSSL/wolfcrypt/src/ge_low_mem.d \
./WolfSSL/wolfcrypt/src/ge_operations.d \
./WolfSSL/wolfcrypt/src/hash.d \
./WolfSSL/wolfcrypt/src/hc128.d \
./WolfSSL/wolfcrypt/src/hmac.d \
./WolfSSL/wolfcrypt/src/integer.d \
./WolfSSL/wolfcrypt/src/logging.d \
./WolfSSL/wolfcrypt/src/md2.d \
./WolfSSL/wolfcrypt/src/md4.d \
./WolfSSL/wolfcrypt/src/md5.d \
./WolfSSL/wolfcrypt/src/memory.d \
./WolfSSL/wolfcrypt/src/misc.d \
./WolfSSL/wolfcrypt/src/pkcs7.d \
./WolfSSL/wolfcrypt/src/poly1305.d \
./WolfSSL/wolfcrypt/src/pwdbased.d \
./WolfSSL/wolfcrypt/src/rabbit.d \
./WolfSSL/wolfcrypt/src/random.d \
./WolfSSL/wolfcrypt/src/ripemd.d \
./WolfSSL/wolfcrypt/src/rsa.d \
./WolfSSL/wolfcrypt/src/sha.d \
./WolfSSL/wolfcrypt/src/sha256.d \
./WolfSSL/wolfcrypt/src/sha512.d \
./WolfSSL/wolfcrypt/src/tfm.d \
./WolfSSL/wolfcrypt/src/wc_port.d 


# Each subdirectory must supply rules for building sources it contributes
WolfSSL/wolfcrypt/src/%.o: ../WolfSSL/wolfcrypt/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -DWOLFSSL_USER_SETTINGS -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\JSON\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\freertos\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\FreeRTOS-Plus-TCP\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\FreeRTOS-Plus-TCP\portable\Compiler\GCC" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\freertos\portable\gcc\ARM_CM3" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\WolfSSL" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



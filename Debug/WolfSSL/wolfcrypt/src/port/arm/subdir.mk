################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../WolfSSL/wolfcrypt/src/port/arm/armv8-32-curve25519.c \
../WolfSSL/wolfcrypt/src/port/arm/armv8-32-sha512-asm.c \
../WolfSSL/wolfcrypt/src/port/arm/armv8-aes.c \
../WolfSSL/wolfcrypt/src/port/arm/armv8-chacha.c \
../WolfSSL/wolfcrypt/src/port/arm/armv8-curve25519.c \
../WolfSSL/wolfcrypt/src/port/arm/armv8-poly1305.c \
../WolfSSL/wolfcrypt/src/port/arm/armv8-sha256.c \
../WolfSSL/wolfcrypt/src/port/arm/armv8-sha512-asm.c \
../WolfSSL/wolfcrypt/src/port/arm/armv8-sha512.c \
../WolfSSL/wolfcrypt/src/port/arm/cryptoCell.c \
../WolfSSL/wolfcrypt/src/port/arm/cryptoCellHash.c 

S_UPPER_SRCS += \
../WolfSSL/wolfcrypt/src/port/arm/armv8-32-curve25519.S \
../WolfSSL/wolfcrypt/src/port/arm/armv8-32-sha512-asm.S \
../WolfSSL/wolfcrypt/src/port/arm/armv8-curve25519.S \
../WolfSSL/wolfcrypt/src/port/arm/armv8-sha512-asm.S 

OBJS += \
./WolfSSL/wolfcrypt/src/port/arm/armv8-32-curve25519.o \
./WolfSSL/wolfcrypt/src/port/arm/armv8-32-sha512-asm.o \
./WolfSSL/wolfcrypt/src/port/arm/armv8-aes.o \
./WolfSSL/wolfcrypt/src/port/arm/armv8-chacha.o \
./WolfSSL/wolfcrypt/src/port/arm/armv8-curve25519.o \
./WolfSSL/wolfcrypt/src/port/arm/armv8-poly1305.o \
./WolfSSL/wolfcrypt/src/port/arm/armv8-sha256.o \
./WolfSSL/wolfcrypt/src/port/arm/armv8-sha512-asm.o \
./WolfSSL/wolfcrypt/src/port/arm/armv8-sha512.o \
./WolfSSL/wolfcrypt/src/port/arm/cryptoCell.o \
./WolfSSL/wolfcrypt/src/port/arm/cryptoCellHash.o 

C_DEPS += \
./WolfSSL/wolfcrypt/src/port/arm/armv8-32-curve25519.d \
./WolfSSL/wolfcrypt/src/port/arm/armv8-32-sha512-asm.d \
./WolfSSL/wolfcrypt/src/port/arm/armv8-aes.d \
./WolfSSL/wolfcrypt/src/port/arm/armv8-chacha.d \
./WolfSSL/wolfcrypt/src/port/arm/armv8-curve25519.d \
./WolfSSL/wolfcrypt/src/port/arm/armv8-poly1305.d \
./WolfSSL/wolfcrypt/src/port/arm/armv8-sha256.d \
./WolfSSL/wolfcrypt/src/port/arm/armv8-sha512-asm.d \
./WolfSSL/wolfcrypt/src/port/arm/armv8-sha512.d \
./WolfSSL/wolfcrypt/src/port/arm/cryptoCell.d \
./WolfSSL/wolfcrypt/src/port/arm/cryptoCellHash.d 


# Each subdirectory must supply rules for building sources it contributes
WolfSSL/wolfcrypt/src/port/arm/%.o: ../WolfSSL/wolfcrypt/src/port/arm/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: MCU Assembler'
	arm-none-eabi-gcc -c -x assembler-with-cpp -D__REDLIB__ -DDEBUG -D__CODE_RED -g3 -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

WolfSSL/wolfcrypt/src/port/arm/%.o: ../WolfSSL/wolfcrypt/src/port/arm/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -DWOLFSSL_USER_SETTINGS -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\JSON\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\freertos\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\FreeRTOS-Plus-TCP\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\FreeRTOS-Plus-TCP\portable\Compiler\GCC" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\freertos\portable\gcc\ARM_CM3" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\WolfSSL" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



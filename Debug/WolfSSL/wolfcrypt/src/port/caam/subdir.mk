################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../WolfSSL/wolfcrypt/src/port/caam/caam_aes.c \
../WolfSSL/wolfcrypt/src/port/caam/caam_driver.c \
../WolfSSL/wolfcrypt/src/port/caam/caam_init.c \
../WolfSSL/wolfcrypt/src/port/caam/caam_sha.c 

OBJS += \
./WolfSSL/wolfcrypt/src/port/caam/caam_aes.o \
./WolfSSL/wolfcrypt/src/port/caam/caam_driver.o \
./WolfSSL/wolfcrypt/src/port/caam/caam_init.o \
./WolfSSL/wolfcrypt/src/port/caam/caam_sha.o 

C_DEPS += \
./WolfSSL/wolfcrypt/src/port/caam/caam_aes.d \
./WolfSSL/wolfcrypt/src/port/caam/caam_driver.d \
./WolfSSL/wolfcrypt/src/port/caam/caam_init.d \
./WolfSSL/wolfcrypt/src/port/caam/caam_sha.d 


# Each subdirectory must supply rules for building sources it contributes
WolfSSL/wolfcrypt/src/port/caam/%.o: ../WolfSSL/wolfcrypt/src/port/caam/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -DWOLFSSL_USER_SETTINGS -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\JSON\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\freertos\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\FreeRTOS-Plus-TCP\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\FreeRTOS-Plus-TCP\portable\Compiler\GCC" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\freertos\portable\gcc\ARM_CM3" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\SW000030_DHCP\WolfSSL" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



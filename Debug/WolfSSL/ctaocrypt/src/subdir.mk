################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../WolfSSL/ctaocrypt/src/aes.c \
../WolfSSL/ctaocrypt/src/des3.c \
../WolfSSL/ctaocrypt/src/fips.c \
../WolfSSL/ctaocrypt/src/fips_test.c \
../WolfSSL/ctaocrypt/src/hmac.c \
../WolfSSL/ctaocrypt/src/misc.c \
../WolfSSL/ctaocrypt/src/random.c \
../WolfSSL/ctaocrypt/src/rsa.c \
../WolfSSL/ctaocrypt/src/sha.c \
../WolfSSL/ctaocrypt/src/sha256.c \
../WolfSSL/ctaocrypt/src/sha512.c \
../WolfSSL/ctaocrypt/src/wolfcrypt_first.c \
../WolfSSL/ctaocrypt/src/wolfcrypt_last.c 

OBJS += \
./WolfSSL/ctaocrypt/src/aes.o \
./WolfSSL/ctaocrypt/src/des3.o \
./WolfSSL/ctaocrypt/src/fips.o \
./WolfSSL/ctaocrypt/src/fips_test.o \
./WolfSSL/ctaocrypt/src/hmac.o \
./WolfSSL/ctaocrypt/src/misc.o \
./WolfSSL/ctaocrypt/src/random.o \
./WolfSSL/ctaocrypt/src/rsa.o \
./WolfSSL/ctaocrypt/src/sha.o \
./WolfSSL/ctaocrypt/src/sha256.o \
./WolfSSL/ctaocrypt/src/sha512.o \
./WolfSSL/ctaocrypt/src/wolfcrypt_first.o \
./WolfSSL/ctaocrypt/src/wolfcrypt_last.o 

C_DEPS += \
./WolfSSL/ctaocrypt/src/aes.d \
./WolfSSL/ctaocrypt/src/des3.d \
./WolfSSL/ctaocrypt/src/fips.d \
./WolfSSL/ctaocrypt/src/fips_test.d \
./WolfSSL/ctaocrypt/src/hmac.d \
./WolfSSL/ctaocrypt/src/misc.d \
./WolfSSL/ctaocrypt/src/random.d \
./WolfSSL/ctaocrypt/src/rsa.d \
./WolfSSL/ctaocrypt/src/sha.d \
./WolfSSL/ctaocrypt/src/sha256.d \
./WolfSSL/ctaocrypt/src/sha512.d \
./WolfSSL/ctaocrypt/src/wolfcrypt_first.d \
./WolfSSL/ctaocrypt/src/wolfcrypt_last.d 


# Each subdirectory must supply rules for building sources it contributes
WolfSSL/ctaocrypt/src/%.o: ../WolfSSL/ctaocrypt/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -DWOLFSSL_USER_SETTINGS -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\JSON\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\freertos\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\FreeRTOS-Plus-TCP\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\FreeRTOS-Plus-TCP\portable\Compiler\GCC" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\freertos\portable\gcc\ARM_CM3" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\wolfssl-freertos-tcp\WolfSSL" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



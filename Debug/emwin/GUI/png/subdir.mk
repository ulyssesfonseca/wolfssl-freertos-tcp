################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../emwin/GUI/png/GUI_PNG.c \
../emwin/GUI/png/IMAGE_PNG.c \
../emwin/GUI/png/adler32.c \
../emwin/GUI/png/compress.c \
../emwin/GUI/png/crc32.c \
../emwin/GUI/png/deflate.c \
../emwin/GUI/png/infback.c \
../emwin/GUI/png/inffast.c \
../emwin/GUI/png/inflate.c \
../emwin/GUI/png/inftrees.c \
../emwin/GUI/png/png.c \
../emwin/GUI/png/pngerror.c \
../emwin/GUI/png/pngget.c \
../emwin/GUI/png/pngmem.c \
../emwin/GUI/png/pngpread.c \
../emwin/GUI/png/pngread.c \
../emwin/GUI/png/pngrio.c \
../emwin/GUI/png/pngrtran.c \
../emwin/GUI/png/pngrutil.c \
../emwin/GUI/png/pngset.c \
../emwin/GUI/png/pngtrans.c \
../emwin/GUI/png/pngwio.c \
../emwin/GUI/png/pngwrite.c \
../emwin/GUI/png/pngwtran.c \
../emwin/GUI/png/pngwutil.c \
../emwin/GUI/png/trees.c \
../emwin/GUI/png/uncompr.c \
../emwin/GUI/png/zutil.c 

OBJS += \
./emwin/GUI/png/GUI_PNG.o \
./emwin/GUI/png/IMAGE_PNG.o \
./emwin/GUI/png/adler32.o \
./emwin/GUI/png/compress.o \
./emwin/GUI/png/crc32.o \
./emwin/GUI/png/deflate.o \
./emwin/GUI/png/infback.o \
./emwin/GUI/png/inffast.o \
./emwin/GUI/png/inflate.o \
./emwin/GUI/png/inftrees.o \
./emwin/GUI/png/png.o \
./emwin/GUI/png/pngerror.o \
./emwin/GUI/png/pngget.o \
./emwin/GUI/png/pngmem.o \
./emwin/GUI/png/pngpread.o \
./emwin/GUI/png/pngread.o \
./emwin/GUI/png/pngrio.o \
./emwin/GUI/png/pngrtran.o \
./emwin/GUI/png/pngrutil.o \
./emwin/GUI/png/pngset.o \
./emwin/GUI/png/pngtrans.o \
./emwin/GUI/png/pngwio.o \
./emwin/GUI/png/pngwrite.o \
./emwin/GUI/png/pngwtran.o \
./emwin/GUI/png/pngwutil.o \
./emwin/GUI/png/trees.o \
./emwin/GUI/png/uncompr.o \
./emwin/GUI/png/zutil.o 

C_DEPS += \
./emwin/GUI/png/GUI_PNG.d \
./emwin/GUI/png/IMAGE_PNG.d \
./emwin/GUI/png/adler32.d \
./emwin/GUI/png/compress.d \
./emwin/GUI/png/crc32.d \
./emwin/GUI/png/deflate.d \
./emwin/GUI/png/infback.d \
./emwin/GUI/png/inffast.d \
./emwin/GUI/png/inflate.d \
./emwin/GUI/png/inftrees.d \
./emwin/GUI/png/png.d \
./emwin/GUI/png/pngerror.d \
./emwin/GUI/png/pngget.d \
./emwin/GUI/png/pngmem.d \
./emwin/GUI/png/pngpread.d \
./emwin/GUI/png/pngread.d \
./emwin/GUI/png/pngrio.d \
./emwin/GUI/png/pngrtran.d \
./emwin/GUI/png/pngrutil.d \
./emwin/GUI/png/pngset.d \
./emwin/GUI/png/pngtrans.d \
./emwin/GUI/png/pngwio.d \
./emwin/GUI/png/pngwrite.d \
./emwin/GUI/png/pngwtran.d \
./emwin/GUI/png/pngwutil.d \
./emwin/GUI/png/trees.d \
./emwin/GUI/png/uncompr.d \
./emwin/GUI/png/zutil.d 


# Each subdirectory must supply rules for building sources it contributes
emwin/GUI/png/%.o: ../emwin/GUI/png/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -D__REDLIB__ -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/emwin/GUI" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/emwin/Inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/emwin/Config" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/emwin/System/HW" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/app/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/board/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/mcu/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/gui_emb/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/panel_touch_FT5216/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/can/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/embnet/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/utilities/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/freertos/inc" -I"/media/ulysses/Doc/PROJETOS/CLIENTES_2/PRATICA/SW000030_PRATICA_IHM_5_POL_TFT/UNIFICADO_CONECTADO/fatfs/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


